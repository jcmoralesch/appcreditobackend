package com.app.tse.model.dao;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import com.app.tse.model.entity.EstadoCuenta;

public interface IEstadoCuentaDao extends PagingAndSortingRepository<EstadoCuenta, Long> {
	
	public EstadoCuenta findByCreditoId(Long id);
	
	@Query("select e from EstadoCuenta e where e.credito.cliente.codigo.ruta.ubicacion=?1 "
			+ " and e.status='P' order by e.id desc") 
	public List<EstadoCuenta> findAllByUbicacion(String ubicacion);
	
	@Query("select e from EstadoCuenta e where e.credito.cliente.codigo.ruta.codigo=?1 and e.credito.mora.diasAdelantado=0 and e.credito.fechaRegistro!=?2 and e.status='P' order by e.id desc")
	public List<EstadoCuenta> findAllByUbicacionForReporte(String ubicacion,LocalDate fecha);
	
	@Query("select e from EstadoCuenta e where e.credito.mora.diasMora>0 and e.status='P' and "
			+ "e.credito.cliente.codigo.ruta.ubicacion=?1")
	public List<EstadoCuenta> findAllByMoraUbicacion(String ubicacion);
	
	@Query("select e from EstadoCuenta e where e.credito.mora.diasAdelantado>0 and e.status='P' and "
			+ "e.credito.cliente.codigo.ruta.ubicacion=?1")
	public List<EstadoCuenta> findAllByAdelantadoUbicacion(String ubicacion);
	
	@Query("select e from EstadoCuenta e where  e.credito.cliente.codigo.ruta.ubicacion=?1  and e.status='P' and"
			+ "e.credito.fechaVencimiento< ?2")
	public List<EstadoCuenta> findAllByFechaVenciciento(String ubicacion,LocalDate fecha);
	
	
}
