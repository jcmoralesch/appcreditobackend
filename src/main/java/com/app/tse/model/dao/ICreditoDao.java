package com.app.tse.model.dao;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.tse.model.entity.Cliente;
import com.app.tse.model.entity.Credito;

public interface ICreditoDao extends JpaRepository<Credito, Long> {
    @Query("select c from Credito c where c.cliente.codigo.ruta.ubicacion=?1 and c.fechaRegistro=?2")
	public List<Credito> findCreditoByUbicacion(String ubicacion,LocalDate fecha);
    
    @Query("select  c from Credito c where c.cliente.codigo.ruta.codigo=?1 and c.fechaRegistro=?2")
    public List<Credito> findCreditoByRuta(String ruta,LocalDate fecha);
    
    public List<Credito> findByFechaRegistroBetween(LocalDate fecha1,LocalDate fecha2);
    
    @Query("select c from Credito c where c.fechaRegistro between ?1 and ?2 "
    		+ "and c.cliente.codigo.ruta.ubicacion=?3")
    public List<Credito> findCreditoByDateAndUbicacion(LocalDate fecha1,LocalDate fecha2, String ubicacion);
    
    @Query("select c from Credito c where c.fechaRegistro between ?1 and ?2 and c.cliente.codigo.ruta.ubicacion=?3 and c.cliente.codigo.ruta.codigo=?4")
    public List<Credito> findCreditoByDateAndUbicacionAndRuta(LocalDate fecha1,LocalDate fecha2,String ubicacion,String ruta);
    
    public Credito findByCliente(Cliente cliente);
    
    @Query("select c from Credito c where c.status='P'")
    public List<Credito> findAllStatusA();
    
    @Query("select c from Credito c where c.cliente.codigo.ruta.codigo=?1 and c.status='P'")
    public List<Credito> findAllStatusAAndRuta(String ruta);
}
