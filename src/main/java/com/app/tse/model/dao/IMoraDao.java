package com.app.tse.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.app.tse.model.entity.Mora;

public interface IMoraDao extends JpaRepository<Mora, Long> {
	public Mora findByCreditoId(Long id);
}
