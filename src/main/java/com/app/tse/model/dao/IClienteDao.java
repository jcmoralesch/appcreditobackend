package com.app.tse.model.dao;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import com.app.tse.model.entity.Cliente;
import com.app.tse.model.entity.TipoPago;

public interface IClienteDao extends PagingAndSortingRepository<Cliente, Long> {
	
	@Query("from TipoPago")
	public List<TipoPago> findAllTipoPago();
	
	@Query("select c from Cliente c where c.codigo.ruta.ubicacion=?1")
	public List<Cliente> findAllByUbicacion(String ubicacion);
	
	@Query("select c from Cliente c where c.codigo.ruta.ubicacion=?1 and c.status='I'")
	public List<Cliente> findAllByUbicacionAndStatus(String ubicacion);
	
	@Query("select c from Cliente c where c.codigo.ruta.ubicacion=?1 and c.status='A'")
	public List<Cliente> findAllByUbicacionAndStatusA(String ubicacion);

}
