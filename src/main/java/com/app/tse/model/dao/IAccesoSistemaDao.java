package com.app.tse.model.dao;

import org.springframework.data.repository.CrudRepository;
import com.app.tse.model.entity.AccesoSistema;

public interface IAccesoSistemaDao extends CrudRepository<AccesoSistema, Long> {

}
