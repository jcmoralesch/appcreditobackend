package com.app.tse.model.dao;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.app.tse.model.entity.Codigo;

public interface ICodigoDao extends JpaRepository<Codigo, Long> {
	
	@Query("select c from Codigo c where c.ruta.ubicacion=?1 and c.status='Libre'")
	public List<Codigo> getAllByAgencia(String ubicacion);
	
	@Query("select c from Codigo c where c.ruta.codigo=?1")
	public List<Codigo> findCodigoByAgencia(String codigo);
	
	@Query("select c from Codigo c where c.ruta.codigo=?1 and c.status='Libre'")
	public List<Codigo> findCodigoLibre(String codigo);

}
