package com.app.tse.model.dao;

import org.springframework.data.repository.CrudRepository;
import com.app.tse.model.entity.Role;

public interface IRoleDao extends CrudRepository<Role, Long> {

}
