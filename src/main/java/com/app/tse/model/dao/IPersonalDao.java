package com.app.tse.model.dao;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.app.tse.model.entity.Personal;
import com.app.tse.model.entity.Ruta;

public interface IPersonalDao extends JpaRepository<Personal, Long> {
	
	@Query("from Ruta")
	public List<Ruta> findAllRuta();
	public Page<Personal> findAllByStatus(String status,Pageable pageable);
	@Query("select p from Personal p where p.ruta.codigo=?1")
	public Personal findPersonalByRuta(String ruta);

}
