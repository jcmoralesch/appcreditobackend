package com.app.tse.model.dao;

import java.time.LocalDate;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import com.app.tse.model.entity.Deposito;

public interface IDepositoDao extends PagingAndSortingRepository<Deposito, Long> {
	
	@Query("select d from Deposito d where d.credito.cliente.codigo.ruta.ubicacion=?1 and d.fechaDeposito=?2 order by d.id desc")
	public List<Deposito> findDepositoByUbicacion(String ubicacion,LocalDate fecha);
	
	@Query("select  d from Deposito d where d.credito.cliente.codigo.ruta.codigo=?1 and d.fechaDeposito=?2")
    public List<Deposito> findDepositoByRuta(String ruta,LocalDate fecha);
	
	public List<Deposito> findByFechaDepositoBetween(LocalDate fecha1,LocalDate fecha2);
	
	@Query("select d from Deposito d where d.fechaDeposito between ?1 and ?2 and d.credito.cliente.codigo.ruta.ubicacion=?3 and d.credito.cliente.codigo.ruta.codigo=?4")
    public List<Deposito> findDepositoByDateAndUbicacionAndRuta(LocalDate fecha1,LocalDate fecha2,String ubicacion,String ruta);
	
	@Query("select d from Deposito d where d.fechaDeposito between ?1 and ?2 and d.credito.cliente.codigo.ruta.ubicacion=?3")
    public List<Deposito> findDepositoByDateAndUbicacion(LocalDate fecha1,LocalDate fecha2, String ubicacion);

}
