package com.app.tse.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.tse.model.entity.TipoPago;

public interface ITipoPagoDao extends JpaRepository<TipoPago, Long> {

}
