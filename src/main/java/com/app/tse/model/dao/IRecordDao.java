package com.app.tse.model.dao;

import org.springframework.data.repository.CrudRepository;
import com.app.tse.model.entity.Cliente;
import com.app.tse.model.entity.Record;

public interface IRecordDao extends CrudRepository<Record, Long> {
	
	public Record findByCliente(Cliente cliente);
    
}
