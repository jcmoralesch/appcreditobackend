package com.app.tse.model.dao;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.app.tse.model.entity.Credito;
import com.app.tse.model.entity.FechaPago;

public interface IFechaPagoDao extends JpaRepository<FechaPago, Long> {
	 @Query("select f from FechaPago f where f.fecha = ?1 and f.credito.id = ?2")
	  Optional<FechaPago> findByFechaAndIdCredito(LocalDate fecha,Long idCredito);
	 
	 @Query("select f from FechaPago f where f.fecha=?1 and f.credito.cliente.codigo.ruta.codigo=?2 "
	 		+ "and f.credito.mora.diasAdelantado=0 and f.credito.estadoCuenta.estadoPago=?3 order by f.credito.id desc")
		public List<FechaPago> findAllByFechaPagoAndRuta(LocalDate fecha,String ruta,String estadoPago);
	  
	 @Query("select f from FechaPago f where f.credito.id=?1")
	 public List<FechaPago> findAllByCreditoId(Long id); 
	 
	 public List<FechaPago> findAllByCredito(Credito credito);
	 
	 @Query("select f from FechaPago f where f.credito.cliente.codigo.ruta.ubicacion=?1 and f.fecha=?2")
	 public List<FechaPago> findByUbicacionAndFechaPago(String ubicacion,LocalDate fecha);

}
