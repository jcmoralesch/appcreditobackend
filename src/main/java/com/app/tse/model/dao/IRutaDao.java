package com.app.tse.model.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.app.tse.model.entity.Ruta;

public interface IRutaDao extends PagingAndSortingRepository<Ruta, Long> {
	
	@Query("select DISTINCT r.ubicacion from Ruta r")
	public List<String> findRutaDistinct();
	
	@Query("select r from Ruta r where r.ubicacion=?1")
	public List<Ruta> findByRutaUbicacion(String ubicacion);

}
