package com.app.tse.model.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.app.tse.model.entity.Personal;
import com.app.tse.model.entity.Usuario;

public interface IUsuarioDao extends CrudRepository<Usuario, Long> {
	
	public Usuario findByUsername(String username);
	public List<Usuario> findAllByStatus(String status);
	public Usuario findByPersonal(Personal personal);

}
