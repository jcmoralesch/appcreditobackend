package com.app.tse.model.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "moras")
public class Mora implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "mora_generada")
	private Double moraGenerada = 0.00;
	@Column(name = "dias_mora")
	private Integer diasMora = 0;
	@Column(name = "total_de_mora")
	private Double totalDeMora = 0.00;
	@Column(name = "dias_adelantado")
	private Integer diasAdelantado = 0;
	@Column(name = "total_adelantado")
	private Double totalAdelantado = 0.00;
	@Column(name = "mini_deposito")
	private Double miniDeposito = 0.00;
	@OneToOne(fetch=FetchType.LAZY)
	@JsonIgnoreProperties({"hibernateLazyInitializer","handler","mora"})
	private Credito credito;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getMoraGenerada() {
		return moraGenerada;
	}

	public void setMoraGenerada(Double moraGenerada) {
		this.moraGenerada = moraGenerada;
	}

	public Integer getDiasMora() {
		return diasMora;
	}

	public void setDiasMora(Integer diasMora) {
		this.diasMora = diasMora;
	}

	public Double getTotalDeMora() {
		return totalDeMora;
	}

	public void setTotalDeMora(Double totalDeMora) {
		this.totalDeMora = totalDeMora;
	}

	public Integer getDiasAdelantado() {
		return diasAdelantado;
	}

	public void setDiasAdelantado(Integer diasAdelantado) {
		this.diasAdelantado = diasAdelantado;
	}

	public Double getTotalAdelantado() {
		return totalAdelantado;
	}

	public void setTotalAdelantado(Double totalAdelantado) {
		this.totalAdelantado = totalAdelantado;
	}

	public Double getMiniDeposito() {
		return miniDeposito;
	}

	public void setMiniDeposito(Double miniDeposito) {
		this.miniDeposito = miniDeposito;
	}

	public Credito getCredito() {
		return credito;
	}

	public void setCredito(Credito credito) {
		this.credito = credito;
	}

}