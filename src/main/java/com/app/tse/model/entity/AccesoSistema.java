package com.app.tse.model.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;


@Entity
@Table(name = "acceso_sistemas")
public class AccesoSistema implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@ManyToOne
	private Usuario usario;
	@Column(name = "fecha_acceso")
	private LocalDate fechaAcceso;
	@Column(name = "hora_acceso")
	private LocalTime horaAcceso;
	
	@PrePersist
	public void prePersist() {
		fechaAcceso = LocalDate.now(ZoneId.of("America/Guatemala"));
		horaAcceso =LocalTime.now(ZoneId.of("America/Guatemala"));
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Usuario getUsario() {
		return usario;
	}

	public void setUsario(Usuario usario) {
		this.usario = usario;
	}

	public LocalDate getFechaAcceso() {
		return fechaAcceso;
	}

	public void setFechaAcceso(LocalDate fechaAcceso) {
		this.fechaAcceso = fechaAcceso;
	}

	public LocalTime getHoraAcceso() {
		return horaAcceso;
	}

	public void setHoraAcceso(LocalTime horaAcceso) {
		this.horaAcceso = horaAcceso;
	}

}
