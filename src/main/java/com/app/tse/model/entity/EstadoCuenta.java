package com.app.tse.model.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "estado_cuentas")
public class EstadoCuenta implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@DecimalMin(value = "0.00")
	private Double saldo = 0.00;
	@Column(name="pagos_faltantes")
	private Integer pagosFaltantes;
	@Column(name="total_pagado")
	private Double totalPagado = 0.00;
	@Column(name="pagos_realizados")
	private Integer pagosRealizados = 0;
	@Column(length = 3)
	private String status = "P";
	@Column(length = 15,name="estado_pago")
	private String estadoPago = "Pendiente";
	@OneToOne(fetch=FetchType.LAZY)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler","estadoCuenta" })
	private Credito credito;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	public Integer getPagosFaltantes() {
		return pagosFaltantes;
	}

	public void setPagosFaltantes(Integer pagosFaltantes) {
		this.pagosFaltantes = pagosFaltantes;
	}

	public Double getTotalPagado() {
		return totalPagado;
	}

	public void setTotalPagado(Double totalPagado) {
		this.totalPagado = totalPagado;
	}

	public Integer getPagosRealizados() {
		return pagosRealizados;
	}

	public void setPagosRealizados(Integer pagosRealizados) {
		this.pagosRealizados = pagosRealizados;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getEstadoPago() {
		return estadoPago;
	}

	public void setEstadoPago(String estadoPago) {
		this.estadoPago = estadoPago;
	}

	public Credito getCredito() {
		return credito;
	}

	public void setCredito(Credito credito) {
		this.credito = credito;
	}

}
