package com.app.tse.model.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "creditos")
public class Credito implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotNull
	@DecimalMin(value = "0.00")
	private Double monto;
	private Double montoTotal;
	private Double cuota;
	@Column(length = 3)
	private String status = "P";
	@Column(name = "fecha_registro")
	private LocalDate fechaRegistro;
	@Column(name = "fecha_vencimiento")
	private LocalDate fechaVencimiento;
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler", "credito" })
	private Cliente cliente;
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler", "credito" })
	private TipoPago tipoPago;
	
	@OneToMany(mappedBy = "credito", cascade = CascadeType.ALL)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler", "credito" })
	private List<FechaPago> fechaPago = new ArrayList<>();  
	@OneToMany(mappedBy = "credito", fetch = FetchType.LAZY)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler", "credito" })
	private List<Deposito> deposito = new ArrayList<>();
	@OneToOne(mappedBy = "credito", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler", "credito" })
	private Mora mora;

	@OneToOne(mappedBy = "credito", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler", "credito" })
	private EstadoCuenta estadoCuenta;
	@ManyToOne
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler", "credito" })
	private Usuario usuario;

	public EstadoCuenta getEstadoCuenta() {
		return estadoCuenta;
	}

	public void setEstadoCuenta(EstadoCuenta estadoCuenta) {
		this.estadoCuenta = estadoCuenta;
	}

	@PrePersist
	public void prePersist() {
		this.fechaRegistro = LocalDate.now(ZoneId.of("America/Guatemala"));
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getMonto() {
		return monto;
	}

	public void setMonto(Double monto) {
		this.monto = monto;
	}

	public Double getMontoTotal() {
		return montoTotal;
	}

	public void setMontoTotal(Double montoTotal) {
		this.montoTotal = montoTotal;
	}

	public Double getCuota() {
		return cuota;
	}

	public void setCuota(Double cuota) {
		this.cuota = cuota;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public LocalDate getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(LocalDate fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public LocalDate getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(LocalDate fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public TipoPago getTipoPago() {
		return tipoPago;
	}

	public void setTipoPago(TipoPago tipoPago) {
		this.tipoPago = tipoPago;
	}
	
	@JsonIgnore
	public List<FechaPago> getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(List<FechaPago> fechaPago) {
		this.fechaPago = fechaPago;
	}  

	public List<Deposito> getDeposito() {
		return deposito;
	}

	public Mora getMora() {
		return mora;
	}

	public void setMora(Mora mora) {
		this.mora = mora;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	

	public void setDeposito(List<Deposito> deposito) {
		this.deposito = deposito;
	}

	public void addMora(Mora mora) {
		mora.setCredito(this);
		this.mora = mora;
	}

	public void removeMora() {
		if (mora != null) {
			mora.setCredito(null);
			this.mora = null;
		}
	}

}