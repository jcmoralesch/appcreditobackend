package com.app.tse.model.service;

import java.time.LocalDate;
import java.util.List;

import com.app.tse.model.entity.Cliente;
import com.app.tse.model.entity.Credito;
import com.app.tse.model.entity.Deposito;
import com.app.tse.model.entity.EstadoCuenta;
import com.app.tse.model.entity.FechaPago;
import com.app.tse.model.entity.Mora;


public interface IDepositoService {
	
	public Deposito store(Deposito deposito,Mora mora,EstadoCuenta estadoCuenta);
	public Deposito store(Cliente cliente,Credito credito,Mora mora,EstadoCuenta estadoCuenta,Deposito deposito,List<FechaPago> fechas);
	public List<Deposito> findDepositoByUbicacion(String ubicacion,LocalDate fecha);
	public List<Deposito> findDepositoByRuta(String ruta,LocalDate fecha);
	public List<Deposito> findByFechaDepositoBetween(LocalDate fecha1,LocalDate fecha2);
	public List<Deposito> findDepositoByDateAndUbicacionAndRuta(LocalDate fecha1,LocalDate fecha2,String ubicacion,String ruta);
	public List<Deposito> findDepositoByDateAndUbicacion(LocalDate fecha1,LocalDate fecha2, String ubicacion);

}