package com.app.tse.model.service;

import java.time.LocalDate;
import java.time.ZoneId;
import org.springframework.stereotype.Component;
import com.app.tse.model.entity.EstadoCuenta;
import com.app.tse.model.entity.Mora;


@Component
public class SinFechaPagoDiarioImpl implements ISinFechaPagoDiarioService{

	@Override
	public void verificarMora(int diasMora, int noPagos, LocalDate fechaRegistro, Mora mora, Double cuota) {

		if(diasMora>noPagos) {
			mora.setTotalDeMora(mora.getTotalDeMora()-(noPagos*cuota));
			mora.setDiasMora(mora.getDiasMora()-noPagos);
		}
		else if(diasMora<noPagos) {	
			if(fechaRegistro.equals(LocalDate.now(ZoneId.of("America/Guatemala")))) {
				mora.setDiasAdelantado(mora.getDiasAdelantado()+noPagos); 
				mora.setTotalAdelantado(mora.getTotalAdelantado()+(cuota*noPagos));
			}
		}
		else if(diasMora==noPagos) {
			mora.setDiasMora(0);
			mora.setTotalDeMora(0.00);
		}
		
	}

	@Override
	public void verificarMiniDeposito(Double miniDeposito, Double cuota, Mora mora, EstadoCuenta estadoCuenta) {
		if(miniDeposito<cuota) {
			mora.setMiniDeposito(miniDeposito);
		}
		else if(miniDeposito.equals(cuota)) {
			mora.setMiniDeposito(0.00);
			estadoCuenta.setPagosFaltantes(estadoCuenta.getPagosFaltantes()-1);
			estadoCuenta.setPagosRealizados(estadoCuenta.getPagosRealizados()+1);
			
			if(mora.getDiasMora()==1) {
				mora.setDiasMora(0);
				mora.setTotalDeMora(0.00);
			}
			else if(mora.getDiasMora()>1) {
				mora.setDiasMora(mora.getDiasMora()-1);
				mora.setTotalDeMora(mora.getTotalDeMora()-cuota);
			}
		}
		else if(miniDeposito>cuota) {
			Double obtenerRestante=miniDeposito%cuota;
			mora.setMiniDeposito(obtenerRestante);
			estadoCuenta.setPagosFaltantes(estadoCuenta.getPagosFaltantes()-1);
			estadoCuenta.setPagosRealizados(estadoCuenta.getPagosRealizados()+1);
			
			if(mora.getDiasMora()==1) {
				mora.setDiasMora(0);
				mora.setTotalDeMora(0.00);
			}
			else if(mora.getDiasMora()>1) {
				mora.setDiasMora(mora.getDiasMora()-1);
				mora.setTotalDeMora(mora.getTotalDeMora()-cuota);
			}
		}
		
	}

}
