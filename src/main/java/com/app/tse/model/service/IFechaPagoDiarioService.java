package com.app.tse.model.service;

import com.app.tse.model.entity.EstadoCuenta;
import com.app.tse.model.entity.Mora;

public interface IFechaPagoDiarioService {
	
	public void verificarMora(int diasMora, int noPago,Mora mora,Double cuota);
	public void	verificarMiniDeposito(Double miniDeposito,Double cuota,Mora mora,EstadoCuenta estadoCuenta);
	public void verificarMoraMenorCuota(Double miniDeposito,Double cuota,Mora mora, EstadoCuenta estadoCuenta);
	

}
