package com.app.tse.model.service;

import java.util.List;
import com.app.tse.model.entity.Cliente;
import com.app.tse.model.entity.TipoPago;


public interface IClienteService  {
	
	public Cliente store(Cliente cliente);
    public List<TipoPago> findAllTipoPago();
    public Cliente findById(Long id);
    public List<Cliente> findAllByUbicacion(String ubicacion);
    public void delete(Cliente cliente);
    public List<Cliente> findAllByUbicacionAndStatus(String ubicacion);
    public List<Cliente> findAllByUbicacionAndStatusA(String ubicacion);

}