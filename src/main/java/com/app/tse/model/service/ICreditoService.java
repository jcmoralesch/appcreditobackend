package com.app.tse.model.service;

import java.time.LocalDate;
import java.util.List;

import com.app.tse.model.entity.Cliente;
import com.app.tse.model.entity.Credito;

public interface ICreditoService {
	
	public Credito store(Credito credito);
	public Credito findById(Long id);
	public List<Credito> findCreditoByUbicacion(String ubicacion,LocalDate fecha);
	public List<Credito> findCreditoByRuta(String ruta,LocalDate fecha);
	public List<Credito> findByFechaPagoBeetween(LocalDate fecha1,LocalDate fecha2);
	public List<Credito> findByDateAndUbicacion(LocalDate fecha1,LocalDate fecha2,String ubicacion);
	public List<Credito> findCreditoByDateAndUbicacionAndRuta(LocalDate fecha1,LocalDate fecha2,String ubicacion,String ruta);
	public Credito findByCliente(Cliente cliente);
	public List<Credito> findAllStatusA();
	public List<Credito> findAllStatusAAndRuta(String ruta);

}