package com.app.tse.model.service;

import java.time.LocalDate;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.tse.model.dao.IEstadoCuentaDao;
import com.app.tse.model.entity.EstadoCuenta;


@Service
public class EstadoCuentaServiceImpl implements IEstadoCuentaService{
	
	@Autowired
	private IEstadoCuentaDao estadoCuentaDao;

	@Override
	public EstadoCuenta store(EstadoCuenta estadoCuenta) {
		
		return estadoCuentaDao.save(estadoCuenta);
	}

	@Override
	public Page<EstadoCuenta> findAll(Pageable pageable) {
		
		 return estadoCuentaDao.findAll(pageable);
	}

	@Override
	public EstadoCuenta findByIdCredito(Long id) {
				
		return estadoCuentaDao.findByCreditoId(id);
	}

	@Override
	public List<EstadoCuenta> findAll() {
		
		return (List<EstadoCuenta>) estadoCuentaDao.findAll();
	}

	@Override
	public List<EstadoCuenta> findAllByUbicacion(String ubicacion) {
		
		return estadoCuentaDao.findAllByUbicacion(ubicacion);
	}

	@Override
	public List<EstadoCuenta> findAllByMoraUbicacion(String ubicacion) {
		
		return estadoCuentaDao.findAllByMoraUbicacion(ubicacion);
	}

	@Override
	public List<EstadoCuenta> findAllByAdelantadoUbicacion(String ubicacion) {
		
		return estadoCuentaDao.findAllByAdelantadoUbicacion(ubicacion);
	}

	@Override
	public List<EstadoCuenta> findAllByFechaVencimiento(String ubicacion,LocalDate fecha) {
		
		return estadoCuentaDao.findAllByFechaVenciciento(ubicacion,fecha);
	}

	@Override
	public EstadoCuenta findById(Long id) {
		
		return estadoCuentaDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public List<EstadoCuenta> findAllByUbicacionForReporte(String ubicacion,LocalDate fecha) {
		
		return estadoCuentaDao.findAllByUbicacionForReporte(ubicacion,fecha);
	}


}
