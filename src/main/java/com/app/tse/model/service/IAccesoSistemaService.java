package com.app.tse.model.service;

import com.app.tse.model.entity.AccesoSistema;

public interface IAccesoSistemaService {
	
	public AccesoSistema store(AccesoSistema accesoSistema);

}
