package com.app.tse.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.app.tse.model.dao.IRutaDao;
import com.app.tse.model.entity.Ruta;


@Service
public class RutaServiceImpl implements IRutaService{
	
	@Autowired
	private IRutaDao rutaDao;

	@Override
	public Ruta store(Ruta ruta) {
		return rutaDao.save(ruta);
	}

	@Override
	public Page<Ruta> findAll(Pageable pageable) {
		return rutaDao.findAll(pageable);
	}

	@Override
	public List<Ruta> findAllRuta() {
		
		return (List<Ruta>) rutaDao.findAll();
	}

	@Override
	public List<String> findRutaDistinct() {
		
		return rutaDao.findRutaDistinct();
	}

	@Override
	public List<Ruta> findRutaByUbicacion(String ubicacion) {
		
		return rutaDao.findByRutaUbicacion(ubicacion);
	}

	

}