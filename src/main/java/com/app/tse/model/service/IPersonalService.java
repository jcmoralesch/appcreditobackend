package com.app.tse.model.service;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.app.tse.model.entity.Personal;
import com.app.tse.model.entity.Ruta;


public interface IPersonalService {
	
	public Personal store(Personal personal);
	public Page<Personal> findAllByStatus(String status,Pageable pageable);
	public List<Ruta> findAllRuta();
	public Personal findById(Long id);
	public void delete(Personal personal);
	public Personal findPersonalByRuta(String ruta);

}
