package com.app.tse.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.tse.model.dao.IAccesoSistemaDao;
import com.app.tse.model.entity.AccesoSistema;

@Service
public class AccesoSistemaImpl implements IAccesoSistemaService{
	
	@Autowired
	private IAccesoSistemaDao accesoSistemaDao;

	@Override
	public AccesoSistema store(AccesoSistema accesoSistema) {
		
		return accesoSistemaDao.save(accesoSistema);
	}

}
