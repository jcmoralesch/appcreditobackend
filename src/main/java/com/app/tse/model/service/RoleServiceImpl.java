package com.app.tse.model.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.app.tse.model.dao.IRoleDao;
import com.app.tse.model.entity.Role;

@Service
public class RoleServiceImpl implements IRoleService{
	@Autowired
	private IRoleDao roleDao;

	@Override
	public List<Role> findAll() {
	
		return (List<Role>) roleDao.findAll();
	}

}
