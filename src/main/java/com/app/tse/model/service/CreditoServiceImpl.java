package com.app.tse.model.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.app.tse.model.dao.ICreditoDao;
import com.app.tse.model.entity.Cliente;
import com.app.tse.model.entity.Credito;


@Service
public class CreditoServiceImpl implements ICreditoService{
	
	@Autowired
	private ICreditoDao creditoDao;

	@Override
    @Transactional
	public Credito store(Credito credito) {
		
		return creditoDao.save(credito);
	}


	@Override
	public Credito findById(Long id) {
		
		return creditoDao.findById(id).orElse(null);
	}

	@Override
	public List<Credito> findCreditoByUbicacion(String ubicacion,LocalDate fecha) {
		
		return creditoDao.findCreditoByUbicacion(ubicacion,fecha);
	}

	@Override
	public List<Credito> findCreditoByRuta(String ruta,LocalDate fecha) {
		
		return creditoDao.findCreditoByRuta(ruta,fecha);
	}

	@Override
	public List<Credito> findByFechaPagoBeetween(LocalDate fecha1, LocalDate fecha2) {
		
		return creditoDao.findByFechaRegistroBetween(fecha1, fecha2);
	}

	@Override
	public List<Credito> findByDateAndUbicacion(LocalDate fecha1, LocalDate fecha2, String ubicacion) {
		
		return creditoDao.findCreditoByDateAndUbicacion(fecha1, fecha2, ubicacion);
	}

	@Override
	public List<Credito> findCreditoByDateAndUbicacionAndRuta(LocalDate fecha1, LocalDate fecha2, String ubicacion,
			String ruta) {
		
		return creditoDao.findCreditoByDateAndUbicacionAndRuta(fecha1, fecha2, ubicacion, ruta);
	}


	@Override
	public Credito findByCliente(Cliente cliente) {
		
		return creditoDao.findByCliente(cliente);
	}


	@Override
	@Transactional(readOnly = true)
	public List<Credito> findAllStatusA() {
		
		return creditoDao.findAllStatusA();
	}


	@Override
	@Transactional(readOnly = true)
	public List<Credito> findAllStatusAAndRuta(String ruta) {
		
		return creditoDao.findAllStatusAAndRuta(ruta);
	}

	

}