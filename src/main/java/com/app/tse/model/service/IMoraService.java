package com.app.tse.model.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.app.tse.model.entity.EstadoCuenta;
import com.app.tse.model.entity.Mora;


public interface IMoraService {
	
	public Mora store(Mora mora);
	public Page<Mora> findAll(Pageable pageable);
	public Mora findByCreditoId(Long id);
	public Mora findById(Long id);
	public List<Mora> storeAll(List<Mora> mora,List<EstadoCuenta> estadoCuenta);

}