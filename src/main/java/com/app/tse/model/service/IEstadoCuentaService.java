package com.app.tse.model.service;

import java.time.LocalDate;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.app.tse.model.entity.EstadoCuenta;



public interface IEstadoCuentaService {
	
	public EstadoCuenta store(EstadoCuenta estadoCuenta);
	public Page<EstadoCuenta> findAll(Pageable pageable);
	public EstadoCuenta findByIdCredito(Long id);
	public List<EstadoCuenta> findAll();
	public List<EstadoCuenta> findAllByUbicacion(String ubicacion);
	public List<EstadoCuenta> findAllByMoraUbicacion(String ubicacion);
	public List<EstadoCuenta> findAllByAdelantadoUbicacion(String ubicacion);
	public List<EstadoCuenta> findAllByFechaVencimiento(String ubicacion,LocalDate fecha);
	public EstadoCuenta findById(Long id);
	public List<EstadoCuenta> findAllByUbicacionForReporte(String ubicacion,LocalDate fecha);
	
}
