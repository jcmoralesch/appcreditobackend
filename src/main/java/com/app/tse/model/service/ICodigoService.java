package com.app.tse.model.service;

import java.util.List;

import com.app.tse.model.entity.Codigo;

public interface ICodigoService {

	public List<Codigo> storeAll(List<Codigo> codigo);
	public List<Codigo> getAllByAgencia(String ubicacion);
	public List<Codigo> findCodigoByAgencia(String codigo);
	public Codigo finById(Long id);
	public Codigo store(Codigo codigo);
	public List<Codigo> findCodigoLibre(String codigo);

}
