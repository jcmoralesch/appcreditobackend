package com.app.tse.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.tse.model.dao.ICodigoDao;
import com.app.tse.model.entity.Codigo;

@Service
public class CodigoServiceImpl implements ICodigoService{
	
	@Autowired
	private ICodigoDao codigoDao;

	@Override
	@Transactional
	public List<Codigo> storeAll(List<Codigo> codigo) {
		
		return codigoDao.saveAll(codigo);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Codigo> getAllByAgencia(String ubicacion) {
	
		return codigoDao.getAllByAgencia(ubicacion);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Codigo> findCodigoByAgencia(String codigo) {
		
		return codigoDao.findCodigoByAgencia(codigo);
	}

	@Override
	@Transactional(readOnly = true)
	public Codigo finById(Long id) {
		
		return codigoDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Codigo store(Codigo codigo) {
		
		return codigoDao.save(codigo);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Codigo> findCodigoLibre(String codigo) {
		
		return codigoDao.findCodigoLibre(codigo);
	}

}
