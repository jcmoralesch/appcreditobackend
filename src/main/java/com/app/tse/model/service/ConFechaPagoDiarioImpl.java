package com.app.tse.model.service;

import org.springframework.stereotype.Component;
import com.app.tse.model.entity.EstadoCuenta;
import com.app.tse.model.entity.Mora;


@Component
public class ConFechaPagoDiarioImpl implements IFechaPagoDiarioService{

	@Override
	public void verificarMora(int diasMora, int noPago, Mora mora, Double cuota) {
		if(diasMora > noPago) {
			mora.setTotalDeMora(mora.getTotalDeMora()-(noPago*cuota));
			mora.setDiasMora(mora.getDiasMora()-noPago);
		}
		else if(diasMora<noPago) {
			if(diasMora==0) {
				mora.setDiasAdelantado(mora.getDiasAdelantado()+noPago);
				mora.setTotalAdelantado(mora.getTotalAdelantado()+(cuota*noPago));
			}
			else {
				mora.setDiasMora(0);
				mora.setTotalDeMora(0.00);
				mora.setDiasAdelantado(noPago-diasMora);
				mora.setTotalAdelantado(((noPago-diasMora)*cuota));
			}
		}
		else if(diasMora==noPago) {
			mora.setDiasMora(0);
			mora.setTotalDeMora(0.00);
		}
		
	}

	@Override
	public void verificarMiniDeposito(Double miniDeposito, Double cuota, Mora mora, EstadoCuenta estadoCuenta) {
		if(miniDeposito<cuota) {
			mora.setMiniDeposito(miniDeposito);
		}
		else if(miniDeposito.equals(cuota)) {
			mora.setMiniDeposito(0.00);
			estadoCuenta.setPagosFaltantes(estadoCuenta.getPagosFaltantes()-1);
			estadoCuenta.setPagosRealizados(estadoCuenta.getPagosRealizados()+1);
			estadoCuenta.setEstadoPago("PAGADO");
			
			if(mora.getDiasMora()==1) {
				mora.setDiasMora(0);
				mora.setTotalDeMora(0.00);
			}
			else if(mora.getDiasMora()>1) {
				mora.setDiasMora(mora.getDiasMora()-1);
				mora.setTotalDeMora(mora.getTotalDeMora()-cuota);
			}
			else if(mora.getDiasMora()==0) {
				mora.setDiasAdelantado(mora.getDiasAdelantado()+1);
				mora.setTotalAdelantado(mora.getTotalAdelantado()+cuota);
			}
		}
		else if(miniDeposito>cuota) {
			Double obtenerRestante=miniDeposito%cuota;
			mora.setMiniDeposito(obtenerRestante);
			
			estadoCuenta.setPagosFaltantes(estadoCuenta.getPagosFaltantes()-1);
			estadoCuenta.setPagosRealizados(estadoCuenta.getPagosRealizados()+1);
			estadoCuenta.setEstadoPago("PAGADO");
			
			if(mora.getDiasMora()==1) {
				mora.setDiasMora(0);
				mora.setTotalDeMora(0.00);
			}
			else if(mora.getDiasMora()>1) {
				mora.setDiasMora(mora.getDiasMora()-1);
				mora.setTotalDeMora(mora.getTotalDeMora()-cuota);
			}
			else if(mora.getDiasMora()==0) {
				mora.setDiasAdelantado(mora.getDiasAdelantado()+1);
				mora.setTotalAdelantado(mora.getTotalAdelantado()+cuota);
			}
		}
		
	}

	@Override
	public void verificarMoraMenorCuota(Double miniDeposito, Double cuota, Mora mora, EstadoCuenta estadoCuenta) {
		if(miniDeposito < cuota) {
			mora.setMiniDeposito(miniDeposito);
			
		}
		else if(miniDeposito==cuota) {
			mora.setMiniDeposito(0.00);
			estadoCuenta.setPagosFaltantes(estadoCuenta.getPagosFaltantes()-1);
			estadoCuenta.setPagosRealizados(estadoCuenta.getPagosRealizados()+1);
			estadoCuenta.setEstadoPago("PAGADO");
		}
		else if(miniDeposito > cuota) {
			Double obtenerRestante=miniDeposito%cuota;
			mora.setMiniDeposito(obtenerRestante);
			
			estadoCuenta.setPagosFaltantes(estadoCuenta.getPagosFaltantes()-1);
			estadoCuenta.setPagosRealizados(estadoCuenta.getPagosRealizados()+1);
			estadoCuenta.setEstadoPago("PAGADO");
			
		}
		
	}

}
