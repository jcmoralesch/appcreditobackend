package com.app.tse.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.tse.model.dao.IPersonalDao;
import com.app.tse.model.entity.Personal;
import com.app.tse.model.entity.Ruta;

@Service
public class PersonalServiceImpl implements IPersonalService {
	
	@Autowired
	private IPersonalDao personalDao;

	@Override
	public Personal store(Personal personal) {
		return personalDao.save(personal);
	}

	@Override
	public Page<Personal> findAllByStatus(String status,Pageable pageable) {
		return personalDao.findAllByStatus(status, pageable);
	}

	@Override
	public List<Ruta> findAllRuta() {
		return personalDao.findAllRuta();
	}

	@Override
	public Personal findById(Long id) {
		
		return personalDao.findById(id).orElse(null);
	}

	@Override
	public void delete(Personal personal) {
		
		personalDao.delete(personal);
		
	}

	@Override
	@Transactional(readOnly = true)
	public Personal findPersonalByRuta(String ruta) {
		
		return personalDao.findPersonalByRuta(ruta);
	}

	

}
