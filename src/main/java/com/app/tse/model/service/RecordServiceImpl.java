package com.app.tse.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.app.tse.model.dao.IRecordDao;
import com.app.tse.model.entity.Cliente;
import com.app.tse.model.entity.Record;

@Service
public class RecordServiceImpl implements IRecordService{
	@Autowired
	private IRecordDao recordDao;

	@Override
	@Transactional(readOnly = true)
	public Record findById(Long id) {
		
		return recordDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Record store(Record record) {
		
		return recordDao.save(record);
	}

	@Override
	@Transactional(readOnly = true)
	public Record findAllByCliente(Cliente cliente) {
		return recordDao.findByCliente(cliente);
	}

}
