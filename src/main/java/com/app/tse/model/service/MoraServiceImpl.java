package com.app.tse.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.tse.model.dao.IEstadoCuentaDao;
import com.app.tse.model.dao.IMoraDao;
import com.app.tse.model.entity.EstadoCuenta;
import com.app.tse.model.entity.Mora;


@Service
public class MoraServiceImpl implements IMoraService{
	
	@Autowired
	private IMoraDao moraDao;
	@Autowired
	private IEstadoCuentaDao estadoCuentaDao;

	@Override
	public Mora store(Mora mora) {
		return moraDao.save(mora);
	}

	@Override
	public Page<Mora> findAll(Pageable pageable) {
		return moraDao.findAll(pageable);
	}

	@Override
	public Mora findByCreditoId(Long id) {
		return moraDao.findByCreditoId(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Mora findById(Long id) {
		return moraDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public List<Mora> storeAll(List<Mora> mora,List<EstadoCuenta> estadoCuenta) {
		
		estadoCuentaDao.saveAll(estadoCuenta);
		
		return moraDao.saveAll(mora);
	}

	

}