package com.app.tse.model.service;

import java.util.List;
import com.app.tse.model.entity.TipoPago;

public interface ITipoPagoService {
	
	public TipoPago store(TipoPago tipoPago);
	public List<TipoPago> findAll();

}
