package com.app.tse.model.service;

import java.util.List;
import com.app.tse.model.entity.Role;

public interface IRoleService {
	
	public List<Role> findAll();

}
