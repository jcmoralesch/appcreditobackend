package com.app.tse.model.service;

import com.app.tse.model.entity.Cliente;
import com.app.tse.model.entity.Record;

public interface IRecordService {
	
	public Record findById(Long id);
	public Record store(Record record);
	public Record findAllByCliente(Cliente cliente);
	

}
