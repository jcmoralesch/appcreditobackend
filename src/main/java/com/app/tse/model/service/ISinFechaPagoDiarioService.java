package com.app.tse.model.service;

import java.time.LocalDate;
import com.app.tse.model.entity.EstadoCuenta;
import com.app.tse.model.entity.Mora;

public interface ISinFechaPagoDiarioService {
	
	public void verificarMora(int diasMora,int noPagos, LocalDate fechaRegistro,Mora mora,Double cuota);
	public void verificarMiniDeposito(Double miniDeposito,Double cuota,Mora mora, EstadoCuenta estadoCuenta);

}
