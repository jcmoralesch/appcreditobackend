package com.app.tse.model.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.tse.model.dao.IClienteDao;
import com.app.tse.model.entity.Cliente;
import com.app.tse.model.entity.TipoPago;


@Service
public class ClienteServiceImpl implements IClienteService{
	
	@Autowired
	private IClienteDao clienteDao;

	@Override
	public Cliente store(Cliente cliente) {
		
		return clienteDao.save(cliente);
	}


	@Override
	public List<TipoPago> findAllTipoPago() {
		
		return clienteDao.findAllTipoPago();
	}


	@Override
	public Cliente findById(Long id) {
		
		return clienteDao.findById(id).orElse(null);
	}


	@Override
	public List<Cliente> findAllByUbicacion(String ubicacion) {
		
		return clienteDao.findAllByUbicacion(ubicacion);
	}


	@Override
	@Transactional
	public void delete(Cliente cliente) {
		
		clienteDao.delete(cliente);
	}


	@Override
	@Transactional(readOnly = true)
	public List<Cliente> findAllByUbicacionAndStatus(String ubicacion) {
		
		return clienteDao.findAllByUbicacionAndStatus(ubicacion);
	}


	@Override
	public List<Cliente> findAllByUbicacionAndStatusA(String ubicacion) {
		
		return clienteDao.findAllByUbicacionAndStatusA(ubicacion);
	}
}