package com.app.tse.model.service;

import java.time.LocalDate;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.app.tse.model.dao.IFechaPagoDao;
import com.app.tse.model.entity.Credito;
import com.app.tse.model.entity.FechaPago;


@Service
public class FechaPagoServiceImpl implements IFechaPagoService {
	
	@Autowired
	private IFechaPagoDao fechaPagoDao;

	@Override
	public  FechaPago findByFechaAndIdCredito(LocalDate fecha, Long id) {
		return fechaPagoDao.findByFechaAndIdCredito(fecha, id).orElse(null);
	}

	@Override
	public List<FechaPago> findAllByFechaPagoAndRuta(LocalDate fecha, String ruta,String estadoPago) {
		
		return fechaPagoDao.findAllByFechaPagoAndRuta(fecha, ruta,estadoPago);
	}

	@Override
	public List<FechaPago> findAllByCreditoId(Long id) {	
		return fechaPagoDao.findAllByCreditoId(id);	
	}

	@Override
	public void deleteFechaInBatch(List<FechaPago> fechas) {
		fechaPagoDao.deleteInBatch(fechas);
	}

	@Override
	@Transactional(readOnly = true)
	public List<FechaPago> findAllByCredito(Credito credito) {
		
		return fechaPagoDao.findAllByCredito(credito);
	}

	@Override
	@Transactional(readOnly = true)
	public List<FechaPago> findByUbicacionAndFechaPago(String ubicacion,LocalDate fecha) {
		
		return fechaPagoDao.findByUbicacionAndFechaPago(ubicacion,fecha);
	}

}
