package com.app.tse.model.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.app.tse.model.entity.Ruta;

public interface IRutaService {
	
	public Ruta store (Ruta ruta);
	public Page<Ruta> findAll(Pageable pageable);
	public List<Ruta> findAllRuta();
	public List<String> findRutaDistinct();
	public List<Ruta> findRutaByUbicacion(String ubicacion);

}