package com.app.tse.model.service;

import java.time.LocalDate;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.tse.model.dao.IClienteDao;
import com.app.tse.model.dao.IDepositoDao;
import com.app.tse.model.dao.IEstadoCuentaDao;
import com.app.tse.model.dao.IFechaPagoDao;
import com.app.tse.model.dao.IMoraDao;
import com.app.tse.model.entity.Cliente;
import com.app.tse.model.entity.Credito;
import com.app.tse.model.entity.Deposito;
import com.app.tse.model.entity.EstadoCuenta;
import com.app.tse.model.entity.FechaPago;
import com.app.tse.model.entity.Mora;


@Service
public class DepositoServiceImpl implements IDepositoService {
	
	@Autowired
	private IDepositoDao depositoDao;
	@Autowired
	private IMoraDao moraDao;
	@Autowired
	private IEstadoCuentaDao estadoCuentaDao;
	@Autowired
	private IClienteDao clienteDao;
	@Autowired
	private IFechaPagoDao fechaPagoDao;
	

	@Override
	@Transactional
	public Deposito store(Deposito deposito,Mora mora,EstadoCuenta estadoCuenta) {
		moraDao.save(mora);
		estadoCuentaDao.save(estadoCuenta);
		return depositoDao.save(deposito);
	}
	
	@Override
	@Transactional
	public Deposito store(Cliente cliente, Credito credito, Mora mora, EstadoCuenta estadoCuenta, 
			Deposito deposito, List<FechaPago> fechas) {
		moraDao.save(mora);
		estadoCuentaDao.save(estadoCuenta);
		clienteDao.save(cliente);
		fechaPagoDao.deleteInBatch(fechas);
		return depositoDao.save(deposito);
	}

	@Override
	public List<Deposito> findDepositoByUbicacion(String ubicacion,LocalDate fecha) {
		
		return depositoDao.findDepositoByUbicacion(ubicacion,fecha);
	}

	@Override
	public List<Deposito> findDepositoByRuta(String ruta,LocalDate fecha) {
		
		return depositoDao.findDepositoByRuta(ruta,fecha);
	}

	@Override
	public List<Deposito> findByFechaDepositoBetween(LocalDate fecha1, LocalDate fecha2) {
		
		return depositoDao.findByFechaDepositoBetween(fecha1, fecha2);
	}

	@Override
	public List<Deposito> findDepositoByDateAndUbicacionAndRuta(LocalDate fecha1, LocalDate fecha2, String ubicacion,
			String ruta) {
		
		return depositoDao.findDepositoByDateAndUbicacionAndRuta(fecha1, fecha2, ubicacion, ruta);
	}

	@Override
	public List<Deposito> findDepositoByDateAndUbicacion(LocalDate fecha1, LocalDate fecha2, String ubicacion) {
		
		return depositoDao.findDepositoByDateAndUbicacion(fecha1, fecha2, ubicacion);
	}



}
