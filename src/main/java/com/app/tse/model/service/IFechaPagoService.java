package com.app.tse.model.service;

import java.time.LocalDate;
import java.util.List;

import com.app.tse.model.entity.Credito;
import com.app.tse.model.entity.FechaPago;


public interface IFechaPagoService {
	
	public FechaPago findByFechaAndIdCredito(LocalDate fecha,Long id);
	public List<FechaPago> findAllByFechaPagoAndRuta(LocalDate fecha,String ruta,String estadoPago);
	public List<FechaPago> findAllByCreditoId(Long id);
	public void deleteFechaInBatch(List<FechaPago> fechas);
	public List<FechaPago> findAllByCredito(Credito credito);
	public List<FechaPago> findByUbicacionAndFechaPago(String ubicacion,LocalDate fecha);

}