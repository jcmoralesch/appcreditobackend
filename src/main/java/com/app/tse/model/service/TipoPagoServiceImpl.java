package com.app.tse.model.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.tse.model.dao.ITipoPagoDao;
import com.app.tse.model.entity.TipoPago;


@Service
public class TipoPagoServiceImpl implements ITipoPagoService{
	
	@Autowired
	private ITipoPagoDao tipoPagoDao;

	@Override
	public TipoPago store(TipoPago tipoPago) {
		return tipoPagoDao.save(tipoPago);
	}

	@Override
	public List<TipoPago> findAll() {
		return tipoPagoDao.findAll();
	}


	

}