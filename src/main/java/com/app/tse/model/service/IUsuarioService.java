package com.app.tse.model.service;

import java.util.List;

import com.app.tse.model.entity.Personal;
import com.app.tse.model.entity.Usuario;

public interface IUsuarioService {
	
	public Usuario findByUsername(String username);
	public Usuario store(Usuario usuario);
	public Usuario findById(Long id);
	public List<Usuario> findAllByStatus(String status);
	public Usuario findByPersonal(Personal personal);
}
