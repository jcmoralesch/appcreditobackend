package com.app.tse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringAppPruebaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringAppPruebaApplication.class, args);
	}

}
