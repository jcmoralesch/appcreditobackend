package com.app.tse.controller;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.tse.model.entity.Cliente;
import com.app.tse.model.entity.Credito;
import com.app.tse.model.entity.Deposito;
import com.app.tse.model.entity.EstadoCuenta;
import com.app.tse.model.entity.FechaPago;
import com.app.tse.model.entity.Mora;
import com.app.tse.model.entity.Usuario;
import com.app.tse.model.service.IClienteService;
import com.app.tse.model.service.ICreditoService;
import com.app.tse.model.service.IDepositoService;
import com.app.tse.model.service.IEstadoCuentaService;
import com.app.tse.model.service.IFechaPagoDiarioService;
import com.app.tse.model.service.IFechaPagoService;
import com.app.tse.model.service.IMoraService;
import com.app.tse.model.service.ISinFechaPagoDiarioService;
import com.app.tse.model.service.UsuarioService;

@RestController
@RequestMapping("/api-rest")
public class DepositoRestController {

	@Autowired
	private IDepositoService depositoService;
	@Autowired
	private IFechaPagoService fechaPagoService;
	@Autowired
	private IMoraService moraService;
	@Autowired
	private IEstadoCuentaService estadoCuentaService;
	@Autowired
	private ICreditoService creditoService;
	@Autowired
	private UsuarioService usuarioService;
	@Autowired
	private IClienteService clienteService;
	@Autowired
	private IFechaPagoDiarioService conFechaPagoDiarioService;
	@Autowired
	private ISinFechaPagoDiarioService sinFechaPagoDiarioService;

	@Secured({ "ROLE_ADMIN", "ROLE_USER" })
	@PostMapping("/deposito")
	public ResponseEntity<?> store(@Valid @RequestBody List<Deposito> arrayDeposito, BindingResult errores,
			OAuth2Authentication authentication) {
		Deposito depositoNew = null;
		Map<String, Object> response = new HashMap<String, Object>();
		Cliente cliente = null;
		

		if (errores.hasErrors()) {
			List<String> errors = errores.getFieldErrors().stream()
					.map(err -> "El campo" + err.getField() + " " + err.getDefaultMessage())
					.collect(Collectors.toList());

			response.put("errors", errors);

			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		try {

			for (Deposito deposito : arrayDeposito) {
				
				Boolean eliminarCredito = false;
				String formaPago = deposito.getCredito().getTipoPago().getFormaPago();
				Mora mora = moraService.findByCreditoId(deposito.getCredito().getId());
				Credito credito = creditoService.findById(deposito.getCredito().getId());
				FechaPago fecha = fechaPagoService.findByFechaAndIdCredito(LocalDate.now(ZoneId.of("America/Guatemala")),
						deposito.getCredito().getId());
				Double cuota = credito.getCuota();
				Double cantidad = deposito.getCantidad();
				Double valor = cantidad / cuota;
				int calcularNoPago = (int) Math.round(Math.floor(valor));
				Double restante = cantidad % cuota;
				Double sumarMiniDeposito = mora.getMiniDeposito() + restante;
				EstadoCuenta estadoCuenta = estadoCuentaService.findByIdCredito(deposito.getCredito().getId());
				Double restarSaldo = estadoCuenta.getSaldo() - cantidad;
				int restarPagoFaltante = estadoCuenta.getPagosFaltantes() - calcularNoPago;
				Double sumarTotalPagado = estadoCuenta.getTotalPagado() + cantidad;
				int sumarPagosRealizados = estadoCuenta.getPagosRealizados() + calcularNoPago;
				LocalDate fechaReg = credito.getFechaRegistro();
				Usuario usuario = usuarioService.findByUsername(authentication.getName());
				int verificarMora = mora.getDiasMora();

				deposito.setUsuario(usuario);
				mora.setMoraGenerada(mora.getMoraGenerada() - deposito.getMora());

				if (formaPago.equals("SEMANAL")) {

				} else if (formaPago.equals("DIARIO")) {
					if (fecha == null) { // CUANDO NO TENGA FECHA DE PAGO

						if (calcularNoPago > 1) {// CUANDO NUMERO DE PAGO ES MAYOR QUE 1
							estadoCuenta.setSaldo(restarSaldo);
							estadoCuenta.setPagosFaltantes(restarPagoFaltante);
							estadoCuenta.setTotalPagado(sumarTotalPagado);
							estadoCuenta.setPagosRealizados(sumarPagosRealizados);

							sinFechaPagoDiarioService.verificarMora(verificarMora, calcularNoPago, fechaReg, mora,
									cuota);

							if (restante > 0.00) {
								sinFechaPagoDiarioService.verificarMiniDeposito(sumarMiniDeposito, cuota, mora,
										estadoCuenta);
							}
						}

						else if (calcularNoPago == 1) {// CUANDO NUMERO DE PAGOS SEA EXACTAMENTE IGUAL A 1
							estadoCuenta.setSaldo(restarSaldo);
							estadoCuenta.setPagosFaltantes(restarPagoFaltante);
							estadoCuenta.setTotalPagado(sumarTotalPagado);
							estadoCuenta.setPagosRealizados(sumarPagosRealizados);

							sinFechaPagoDiarioService.verificarMora(verificarMora, calcularNoPago, fechaReg, mora,
									cuota);

							if (restante > 0.00) {
								sinFechaPagoDiarioService.verificarMiniDeposito(sumarMiniDeposito, cuota, mora,
										estadoCuenta);
							}

						} else if (calcularNoPago < 1) {// CUANDO PAGO SEA MENOR A 1 CUOTA
							estadoCuenta.setSaldo(restarSaldo);
							estadoCuenta.setTotalPagado(sumarTotalPagado);

							if (fechaReg.equals(LocalDate.now(ZoneId.of("America/Guatemala")))) {
								estadoCuenta.setEstadoPago("PAGADO");
							}
							sinFechaPagoDiarioService.verificarMiniDeposito(sumarMiniDeposito, cuota, mora,
									estadoCuenta);
						}
					}

					else {// CUANDO TENGA SU FECHA DE PAGO

						if (calcularNoPago > 1) {// CUANDO NUMERO DE PAGOS SEA MAYOR A 1
							estadoCuenta.setSaldo(restarSaldo);
							estadoCuenta.setPagosFaltantes(restarPagoFaltante);
							estadoCuenta.setTotalPagado(sumarTotalPagado);
							estadoCuenta.setPagosRealizados(sumarPagosRealizados);
							estadoCuenta.setEstadoPago("PAGADO");

							int restarNoPagos = calcularNoPago - 1;
							conFechaPagoDiarioService.verificarMora(verificarMora, restarNoPagos, mora, cuota);
							if (restante > 0.00) {
								conFechaPagoDiarioService.verificarMiniDeposito(sumarMiniDeposito, cuota, mora,
										estadoCuenta);
							}
						} else if (calcularNoPago == 1) {// CUANDO EL NUMERO DE PAGO SEA EXACTAMENTE IGUAL A 1
							estadoCuenta.setSaldo(restarSaldo);
							estadoCuenta.setPagosFaltantes(restarPagoFaltante);
							estadoCuenta.setTotalPagado(sumarTotalPagado);
							estadoCuenta.setPagosRealizados(sumarPagosRealizados);
							estadoCuenta.setEstadoPago("PAGADO");

							if (restante > 0.00) {
								conFechaPagoDiarioService.verificarMiniDeposito(sumarMiniDeposito, cuota, mora,
										estadoCuenta);
							}

						} else if (calcularNoPago < 1) {// CUANDO LA CUOTA SEA MENOR A 1
							estadoCuenta.setSaldo(restarSaldo);
							estadoCuenta.setTotalPagado(sumarTotalPagado);
							estadoCuenta.setEstadoPago("PAGADO");
							conFechaPagoDiarioService.verificarMoraMenorCuota(sumarMiniDeposito, cuota, mora,
									estadoCuenta);
						}
					}
				}

				if (estadoCuenta.getSaldo() == 0.00 && mora.getMoraGenerada() == 0.00) {
					eliminarCredito = true;
					cliente = clienteService.findById(credito.getCliente().getId());
					credito.setStatus("C");
					estadoCuenta.setStatus("C");
					cliente.setStatus("I");
				}

				if (eliminarCredito) {
					depositoNew = depositoService.store(cliente, credito, mora, estadoCuenta, deposito,
							fechaPagoService.findAllByCreditoId(credito.getId()));
				} else {
					depositoNew = depositoService.store(deposito, mora, estadoCuenta);
				}
			}
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar el insert en la BBDD");
			response.put("err", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "Deposito registrado Correctamente");
		response.put("deposito", depositoNew);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}

	@Secured({ "ROLE_ADMIN", "ROLE_USER" })
	@GetMapping("/deposito/{ubicacion}")
	public List<Deposito> findDepositoByUbicacion(@PathVariable String ubicacion) {

		return depositoService.findDepositoByUbicacion(ubicacion,LocalDate.now(ZoneId.of("America/Guatemala")));
	}

	@Secured({ "ROLE_ADMIN", "ROLE_USER" })
	@GetMapping("/deposito/ruta/{ruta}")
	public List<Deposito> findDepositoByRuta(@PathVariable String ruta) {

		return depositoService.findDepositoByRuta(ruta,LocalDate.now(ZoneId.of("America/Guatemala")));
	}

	@Secured({ "ROLE_ADMIN" })
	@GetMapping("/deposito/consultar/{fecha1}/{fecha2}/{ubicacion}/{ruta}")
	public List<Deposito> findByDates(@PathVariable("fecha1") String fecha1, @PathVariable("fecha2") String fecha2,
			@PathVariable("ubicacion") String ubicacion, @PathVariable("ruta") String ruta) {

		LocalDate f1 = LocalDate.parse(fecha1);
		LocalDate f2 = LocalDate.parse(fecha2);

		if (ruta.equals("undefined")) {
			ruta = "";
		}
		if (ubicacion.equals("undefined")) {
			ubicacion = "";
		}

		if (ubicacion.equals("") && ruta.equals("")) {

			return depositoService.findByFechaDepositoBetween(f1, f2);
		}

		if (ubicacion != "" && ruta != "") {

			return depositoService.findDepositoByDateAndUbicacionAndRuta(f1, f2, ubicacion, ruta);
		}

		if (ubicacion != "" && ruta.equals("")) {
			return depositoService.findDepositoByDateAndUbicacion(f1, f2, ubicacion);

		}

		if (ubicacion.equals("") && ruta != "") {
			return depositoService.findDepositoByDateAndUbicacionAndRuta(f1, f2, ubicacion, ruta);
		}

		return null;
	}

}




