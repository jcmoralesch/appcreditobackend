package com.app.tse.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.tse.model.entity.Role;
import com.app.tse.model.entity.Usuario;
import com.app.tse.model.service.IUsuarioService;

@RestController
@RequestMapping("/api-credito")
public class UsuarioRestController {
	
	@Autowired
	private IUsuarioService usuarioService;
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	@Secured({"ROLE_ADMIN"})
	@PostMapping("/usuario")
	public ResponseEntity<?> store(@Valid @RequestBody Usuario usuario,BindingResult errores){
		
		Usuario usuarioNew =null;
		Map<String, Object> response = new HashMap<>();
		
		if(errores.hasErrors()) {
			List<String> errors=errores.getFieldErrors().stream().map(err->"El campo " + err.getField()
			+" " + err.getDefaultMessage()).collect(Collectors.toList());
			response.put("errors", errors);
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.BAD_REQUEST);
		}
		
		try {
			
			String passCifrado=passwordEncoder.encode(usuario.getPassword());
			usuario.setPassword(passCifrado);
			
			for (Role role : usuario.getRole()) {
				System.out.println(role);
			}
			
			usuarioNew = usuarioService.store(usuario);	
			
		}
		catch(DataAccessException e) {
			
			response.put("mensaje", "Error al realizar en insert a la BBDD");
			response.put("errors", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje","Usuario creado con exito");
		response.put("usuario", usuarioNew);
		
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);	
		
	}
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping("/usuario/{id}")
	public Usuario findById(@PathVariable Long id) {
		return usuarioService.findById(id);
	}
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping("/usuario/ubicacion/{id}")
	public ResponseEntity<?> findUbicacion(@PathVariable Long id,OAuth2Authentication auth) {
		
		if(id==0) {
			 Usuario us=usuarioService.findByUsername(auth.getName());
			 id=us.getId();
		}
		
		Map<String,Object> response= new HashMap<>();
		
		Usuario usuario= usuarioService.findById(id);
		String usuarioUbicacion=usuario.getPersonal().getRuta().getUbicacion();
		
		response.put("ubicacion", usuarioUbicacion );
		
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.ACCEPTED);
		 
	}
	
	@Secured("ROLE_ADMIN")
	@GetMapping("/usuario")
	public List<Usuario>findAll(){
		
		return usuarioService.findAllByStatus("Activo");
	}
	
	@Secured("ROLE_ADMIN")
	@PutMapping("/usuario/eliminar/{id}")
	public ResponseEntity<?> updateStatus(@PathVariable Long id,@RequestBody Usuario usuario) {
		
		Map<String,Object> response= new HashMap<>();
		Usuario usuarioUpdate=usuarioService.findById(id);
		try {
			usuarioUpdate.setPassword("@123Inactivo");
			usuarioUpdate.setStatus("Inactivo");
			usuarioUpdate.setEnabled(false);
			usuarioService.store(usuarioUpdate);
			
		}catch(DataAccessException e) {
			response.put("mensaje","Error no se puede eliminar");
			response.put("err",usuario.getUsername().concat(" Tiene operaciones realizadas en la base de datos"));
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje", "Usuario eliminado con exito");
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.NO_CONTENT);		
	}
	
	@Secured("ROLE_ADMIN")
	@PutMapping("/usuario/{id}")
	public Usuario update(@PathVariable Long id, @RequestBody Usuario usuario) {
		
		Usuario actualizarUsuario= usuarioService.findById(id);
		
		actualizarUsuario.setRole(usuario.getRole());
		usuarioService.store(actualizarUsuario);
		
		return actualizarUsuario;
		
		
	}
	
	@Secured("ROLE_ADMIN")
	@PutMapping("/usuario/enabled/false-true/{id}/{val}")
	public Usuario updateEnabled(@PathVariable("id") Long id, @PathVariable("val") Integer val, @RequestBody Usuario usuario) {
		Usuario actualizarUsuario= usuarioService.findById(id);
		
		if(val==0) {
			actualizarUsuario.setEnabled(false);
		}
		else {
			actualizarUsuario.setEnabled(true);
		}	
		
		return usuarioService.store(actualizarUsuario);	
	}
	
	@Secured({"ROLE_USER","ROLE_ADMIN"})
	@GetMapping("/usuario/actualizar/{username}")
	public Usuario getByUsername(@PathVariable String username) {
		return usuarioService.findByUsername(username);
	}
	
	@Secured({"ROLE_USER","ROLE_ADMIN"})
	@PutMapping("/usuario/update/{id}")
	public Usuario updateUser(@PathVariable Long id, @RequestBody Usuario usuario) {
		Usuario updatePassUsuario=usuarioService.findById(id);
		
		updatePassUsuario.setUsername(usuario.getUsername());
		String passCifrado=passwordEncoder.encode(usuario.getPassword());
		updatePassUsuario.setPassword(passCifrado);
		
		usuarioService.store(updatePassUsuario);
		
		return updatePassUsuario;
	}
}
