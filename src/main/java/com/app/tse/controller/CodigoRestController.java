package com.app.tse.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.app.tse.model.entity.Codigo;
import com.app.tse.model.service.ICodigoService;

@RestController
@RequestMapping("/api-credito")
public class CodigoRestController {
	
	@Autowired
	private ICodigoService codigoService;
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@PostMapping("/codigo")
	public ResponseEntity<?> store(@Valid @RequestBody List<Codigo> codigos,BindingResult errores){
		
		Map<String, Object> response= new HashMap<>();
		
		if(errores.hasErrors()) {
			List<String> errors=errores.getFieldErrors().stream().map(err->"El campo "+ err.getField() +
					" " + err.getDefaultMessage()).collect(Collectors.toList());
			
			response.put("errors", errors);		
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.BAD_REQUEST);		
		}
		try {
			codigoService.storeAll(codigos);
		}
	    catch(DataAccessException e) {
			
			response.put("mensaje","Error al realizar el isert en la BBDD");
			response.put("errors",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);		
		}
		
		response.put("codigo", "codigo");
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
		
	}
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping("/codigo/agencia/{ubicacion}")
	public List<Codigo> getAllByAgencia(@PathVariable String ubicacion){
		return codigoService.getAllByAgencia(ubicacion);
	}
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping("/codigo/codigo/{codigo}")
	public List<Codigo> findCodigoByAgencia(@PathVariable String codigo){
		return codigoService.findCodigoByAgencia(codigo);
	}
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping("/codigo/libre/{codigo}")
	public List<Codigo> findAllCodigoLibre(@PathVariable String codigo){
		return codigoService.findCodigoLibre(codigo);
	}
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@PutMapping("/codigo/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Codigo updateCodigo(@PathVariable Long id) {
		Codigo codigo=codigoService.finById(id);
		
		codigo.setStatus("Libre");
		return codigoService.store(codigo);
	}
	
	

}
