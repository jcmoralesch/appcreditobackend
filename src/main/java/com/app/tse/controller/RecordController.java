package com.app.tse.controller;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.tse.model.entity.Credito;
import com.app.tse.model.entity.Record;
import com.app.tse.model.service.ICreditoService;
import com.app.tse.model.service.IRecordService;

@RestController
@RequestMapping("/api-credito")
public class RecordController {
	
	@Autowired
	private IRecordService recordService;
	@Autowired
	private ICreditoService creditoService;
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@PutMapping("/record/{id}")
	public Record update(@PathVariable Long id,@RequestBody Record record) {
		
		Record recordUpdated=recordService.findById(id);
		
		recordUpdated.setCiclo(record.getCiclo());
		
		return recordService.store(recordUpdated);
	}
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping("/record/{ubicacion}")
	public List<Record> findRecordByClienteId(@PathVariable String ubicacion){
		
		List<Record> recordCliente= new ArrayList<Record>();
		
		List<Credito> credito=creditoService.findCreditoByUbicacion(ubicacion,LocalDate.now(ZoneId.of("America/Guatemala")));
		
		for (Credito credito2 : credito) {
			recordCliente.add(recordService.findAllByCliente(credito2.getCliente()));
		}
		
		return recordCliente;
		
	}
}
