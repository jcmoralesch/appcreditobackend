package com.app.tse.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.tse.model.entity.TipoPago;
import com.app.tse.model.service.ITipoPagoService;

@RestController
@RequestMapping("/api-credito-tipoPago")
public class TipoPagoRestController {
	@Autowired
	private ITipoPagoService tipoPagoService;
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping("/tipoPago")
	public List<TipoPago> index(){
		
		return tipoPagoService.findAll();
	}
	
	@Secured({"ROLE_ADMIN"})
	@PostMapping("/tipoPago")
	public ResponseEntity<?> store(@Valid @RequestBody TipoPago tipoPago,BindingResult errores) {
		TipoPago tipoPagoNew = null;
		Map<String,Object> response= new HashMap<>();
		
		if(errores.hasErrors()) {
			List<String> errors=errores.getFieldErrors().stream().map(err->"El campo "+ err.getField() 
			+" " + err.getDefaultMessage()).collect(Collectors.toList());
			
			response.put("errors", errors);
			return  new ResponseEntity<Map<String,Object>>(response,HttpStatus.BAD_REQUEST);
		}
		
		try {
			tipoPago.setNombreTipo(tipoPago.getNombreTipo().toUpperCase());
			tipoPago.setPlazo(tipoPago.getPlazo().toUpperCase());
			tipoPagoNew=tipoPagoService.store(tipoPago);
		}
		catch(DataIntegrityViolationException ex) {
			response.put("mensaje", "Error al registrar");
			response.put("err", tipoPago.getNombreTipo()+" Ya esta registrada");
			
			return new ResponseEntity<Map<String,Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		catch(DataAccessException e) {
			response.put("mensaje","Error al realizar el insert en la base de datos");
			response.put("err", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			
			return new ResponseEntity<Map<String,Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			
		}	
		response.put("mensaje","Registro ingresado con exito");
		response.put("tipoPago", tipoPagoNew);
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);			
	}

}
