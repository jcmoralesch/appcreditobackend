package com.app.tse.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.app.tse.model.entity.Cliente;
import com.app.tse.model.entity.Codigo;
import com.app.tse.model.entity.Credito;
import com.app.tse.model.entity.Record;
import com.app.tse.model.entity.TipoPago;
import com.app.tse.model.service.IClienteService;
import com.app.tse.model.service.ICodigoService;
import com.app.tse.model.service.ICreditoService;
import com.app.tse.model.service.IRecordService;

@RestController
@RequestMapping("/api-credito")
public class ClienteRestController {
	
	@Autowired
	private IClienteService clienteService;
	@Autowired 
	private IRecordService recordService;
	@Autowired
	private ICodigoService codigoService;
	@Autowired
	private ICreditoService creditoService;
	
	@Secured({"ROLE_USER","ROLE_ADMIN"})
	@GetMapping("/cliente/tipo-pago")
	public List<TipoPago> findAllTipoPago(){
		
		return clienteService.findAllTipoPago();
	}
	
	@Secured({"ROLE_USER","ROLE_ADMIN"})
	@PostMapping("/cliente")
	public ResponseEntity<?> store(@Valid @RequestBody Cliente cliente,BindingResult errores){
		Cliente clienteNew =null;
		Map<String, Object> response = new HashMap<>();
		
		if(errores.hasErrors()) {
			List<String> errors=errores.getFieldErrors().stream().map(err->"El campo "+ err.getField() +
				" " + err.getDefaultMessage()).collect(Collectors.toList());
			
			response.put("errors",errors);
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.BAD_REQUEST);
		}
		
		try {
		    cliente.setNombre(cliente.getNombre().toUpperCase());
		    cliente.setApellido(cliente.getApellido().toUpperCase());
		    cliente.setStatus("A");
			Record record= new Record();
			cliente.addRecord(record);
			clienteNew =clienteService.store(cliente);
			
			Codigo codigo= codigoService.finById(cliente.getCodigo().getId());
			
			codigo.setStatus("Ocupado");
			codigoService.store(codigo);				
		}
		catch(DataAccessException e) {
			
			response.put("mensaje","Error al realizar el isert en la BBDD");
			response.put("errors",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);		
		}
		
		response.put("mensaje","Cliente registrado con exito");
		response.put("cliente", clienteNew);
		return  new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
	}
	
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping("/cliente/{ubicacion}")
	public List<Cliente> finAll(@PathVariable String ubicacion){
		
		return clienteService.findAllByUbicacion(ubicacion);
	}
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@PutMapping("/cliente/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Cliente update(@PathVariable Long id) {
		Cliente cliente=clienteService.findById(id);
		
		cliente.setStatus("A");
		int ciclo=cliente.getRecord().getCiclo()+1;
	    
		Record record =recordService.findById(cliente.getRecord().getId());
		record.setCiclo(ciclo);
		cliente.addRecord(record);
		return clienteService.store(cliente);
		
	}
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping("/cliente/form/{id}")
	public Cliente fidById(@PathVariable Long id) {
		
		return clienteService.findById(id);
	}
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@PutMapping("/cliente/update/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Cliente updateDatos(@RequestBody Cliente cliente,@PathVariable Long id) {
		Cliente clienteDatos =clienteService.findById(id);
		clienteDatos.setCodigo(cliente.getCodigo());
		clienteDatos.setNombre(cliente.getNombre().toUpperCase());
		clienteDatos.setApellido(cliente.getApellido().toUpperCase());
		clienteDatos.setDireccion(cliente.getDireccion().toUpperCase());
		clienteDatos.setTelefono(cliente.getTelefono());
		clienteService.store(clienteDatos);
		
		Codigo codigo=codigoService.finById(cliente.getCodigo().getId());
		codigo.setStatus("Ocupado");
		codigoService.store(codigo);
		
		return clienteDatos;
		
	}
	
	@Secured("ROLE_ADMIN")
	@DeleteMapping("/cliente/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		
		Map<String,Object> response= new HashMap<>();
		Cliente cliente=clienteService.findById(id);
		Credito credito=creditoService.findByCliente(cliente);
		Codigo codigo=codigoService.finById(cliente.getCodigo().getId());
		
		codigo.setStatus("Libre");
		credito.setCliente(null);
		
		try {
			
			creditoService.store(credito);
			clienteService.delete(cliente);
			codigoService.store(codigo);
		}catch(DataAccessException e) {
			response.put("mensaje","Error no se puede eliminar");
			response.put("err",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje", "Usuario eliminado con exito");
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.NO_CONTENT);		
	}
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping("/cliente/status/{ubicacion}")
	public List<Cliente> findAllByUbicacionAndStatus(@PathVariable String ubicacion){
		return clienteService.findAllByUbicacionAndStatus(ubicacion);
	}
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping("/cliente/status/active/{ubicacion}")
	public List<Cliente> findAllByUbicacionAndStatusA(@PathVariable String ubicacion){
		return clienteService.findAllByUbicacionAndStatusA(ubicacion);
	}
	
}