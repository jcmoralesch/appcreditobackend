package com.app.tse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.app.tse.model.entity.Mora;
import com.app.tse.model.service.IMoraService;

@RestController
@RequestMapping("/api-credito")
public class MoraRestController {
	
	@Autowired
	private IMoraService moraService;
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@PutMapping("/mora/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Mora updateMora(@PathVariable Long id,@RequestBody Mora mora){
		
		Mora moraUpdated=moraService.findById(id);
		
		moraUpdated.setDiasAdelantado(mora.getDiasAdelantado());
		moraUpdated.setDiasMora(mora.getDiasMora());
		moraUpdated.setMoraGenerada(mora.getMoraGenerada());
		moraUpdated.setTotalAdelantado(mora.getTotalAdelantado());
		moraUpdated.setTotalDeMora(mora.getTotalDeMora());
		
		return moraService.store(moraUpdated);
	}

}
