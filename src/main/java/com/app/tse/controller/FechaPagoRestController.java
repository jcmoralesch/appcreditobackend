package com.app.tse.controller;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.tse.model.entity.Credito;
import com.app.tse.model.entity.EstadoCuenta;
import com.app.tse.model.entity.FechaPago;
import com.app.tse.model.entity.Mora;
import com.app.tse.model.service.IFechaPagoService;
import com.app.tse.model.service.IMoraService;

@CrossOrigin(origins = {"http://localhost:4200","*"})
@RestController
@RequestMapping("/api-credito")
public class FechaPagoRestController {
	
	@Autowired
	private IFechaPagoService fechaPagoService;
	@Autowired
	private IMoraService moraService;
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping("/fecha-pago/{ruta}")
	public List<FechaPago> findAllByFechaAndRuta(@PathVariable String ruta, OAuth2Authentication auth){
		
		String estadoPago="Pendiente";
		
		return fechaPagoService.findAllByFechaPagoAndRuta(LocalDate.now(ZoneId.of("America/Guatemala")), ruta,estadoPago);
		
	}
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping("/fecha-pago/finalizar/pago/{ubicacion}")
	public ResponseEntity<?> finalizarPago(@PathVariable String ubicacion){
		Map<String, Object> response=new HashMap<>();
		List<FechaPago> creditoConFechaPago=fechaPagoService.findByUbicacionAndFechaPago(ubicacion,LocalDate.now(ZoneId.of("America/Guatemala")));
		List<Mora> addMora= new ArrayList<Mora>();
		List<EstadoCuenta> addEstadoCuenta= new ArrayList<EstadoCuenta>();
		
		for (FechaPago fechaPago : creditoConFechaPago) {
			EstadoCuenta esCuenta=fechaPago.getCredito().getEstadoCuenta();
			Credito credito =fechaPago.getCredito();
			Mora mora=fechaPago.getCredito().getMora();
			
			if(esCuenta.getEstadoPago().equals("Pendiente")) {
				if(mora.getDiasAdelantado()>0) {
					mora.setDiasAdelantado(mora.getDiasAdelantado()-1); 
					mora.setTotalAdelantado(mora.getTotalAdelantado()-credito.getCuota());
				}
				else if(mora.getDiasAdelantado()==0){
					mora.setDiasMora(mora.getDiasMora()+1);
					mora.setTotalDeMora(mora.getTotalDeMora()+credito.getCuota());
					mora.setMoraGenerada(mora.getMoraGenerada()+(credito.getMonto()*0.01));
				}	
			}
			esCuenta.setEstadoPago("Pendiente");
			addEstadoCuenta.add(esCuenta);
			addMora.add(mora);
			
		}
		
		try {
			moraService.storeAll(addMora,addEstadoCuenta);
		}
		catch (DataAccessException e) {
			response.put("mensaje", "Error, no fue posible realizar la operacion deseada intente nuevamente");
			response.put("err",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("fechaPago", "fechaPago");
		
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
		
	}

}
