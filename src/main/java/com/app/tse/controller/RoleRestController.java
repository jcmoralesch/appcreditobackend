package com.app.tse.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.tse.model.entity.Role;
import com.app.tse.model.service.IRoleService;

@RestController
@RequestMapping("api-credito")
public class RoleRestController {
	
	@Autowired
	private IRoleService roleService;
	
	@Secured({"ROLE_ADMIN"})
	@GetMapping("/role")
	public List<Role> findAll(){
		return roleService.findAll();
	}
		
	

}
