package com.app.tse.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.tse.model.entity.Personal;
import com.app.tse.model.entity.Ruta;
import com.app.tse.model.entity.Usuario;
import com.app.tse.model.service.IPersonalService;
import com.app.tse.model.service.IUsuarioService;

@RestController
@RequestMapping("/api-credito-personal")
public class PersonalRestController {
	
	@Autowired
	private IPersonalService personalService;
	@Autowired
	private IUsuarioService usuarioService;
	
	@Secured({"ROLE_ADMIN"})
	@PostMapping("/personal")
	public ResponseEntity<?> store(@Valid @RequestBody Personal personal,BindingResult errores) {
		
		Personal personalNew=null;
		Map<String, Object> response = new HashMap<>();
		
		if(errores.hasErrors()) {
			List<String> errors=errores.getFieldErrors().stream().map(err-> "El campo " + err.getField()
			+ " " + err.getDefaultMessage()).collect(Collectors.toList());
			
			response.put("errors",errors);
			return new ResponseEntity<Map<String,Object>>(response, HttpStatus.BAD_REQUEST);
		}
		
		try {
			personal.setNombre(personal.getNombre().toUpperCase());
			personal.setApellido(personal.getApellido().toUpperCase());
			personal.setDireccion(personal.getDireccion().toUpperCase());
			personalNew=personalService.store(personal);
		}
		catch(DataIntegrityViolationException ex) {
			response.put("mensaje","Error al registrar");
			response.put("err","La identificación " +personal.getIdentificacion().concat(" ya se encuentra registrada"
					+ "Y posiblemente este tratando de asignar una ruta que ya esta ASIGNADA"));
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		catch(DataAccessException e) {
			response.put("mensaje","Error al realizar el insert en la base de datos");
			response.put("err", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje", "Cliente registrado con exito");
		response.put("personal", personalNew);
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
		
	}
	
	@Secured({"ROLE_ADMIN"})
	@GetMapping("/personal/page/{page}")
	public Page<Personal>findAll(@PathVariable Integer page){

		return personalService.findAllByStatus("A", PageRequest.of(page,10));	
	}
	
	@Secured({"ROLE_ADMIN"})
	@GetMapping("/personal/rutas")
	public List<Ruta>ListardRutas(){
		return personalService.findAllRuta();	
	}
	
	@Secured({"ROLE_ADMIN"})
	@GetMapping("/personal/{id}")
	public Personal findById(@PathVariable Long id) {
		return personalService.findById(id);
	}
	
	@Secured({"ROLE_ADMIN"})
	@PutMapping("/personal/update/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody Personal personal,@PathVariable Long id ) {
		Personal personalDatos=personalService.findById(id);
		
		Map<String,Object> response= new HashMap<>();
		
		personalDatos.setNombre(personal.getNombre().toUpperCase());
		personalDatos.setApellido(personal.getApellido().toUpperCase());
		personalDatos.setDireccion(personal.getDireccion().toUpperCase());
		personalDatos.setRuta(personal.getRuta());
		
		try {
			personalService.store(personalDatos);
		}
		catch(DataIntegrityViolationException ex) {
			
			response.put("mensaje","Error al actualizar");
			response.put("err", "La ruta ".concat(personal.getRuta().getCodigo()).concat("  Esta ASIGNADA"));
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		catch(DataAccessException e) {
			
			response.put("mensaje","Error al realizar el insert en la base de datos");
			response.put("err", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		
		response.put("mensaje","Datos actualizados con éxito");
		response.put("personal", personalDatos);
		
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
	}
	
	
	@Secured("ROLE_ADMIN")
	@DeleteMapping("/personal/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id){
		
		Map<String, Object> response = new HashMap<>();
		Personal persDelete=personalService.findById(id);	
		Usuario usuario=usuarioService.findByPersonal(persDelete);
		
		try {	
			if(usuario==null) {
				personalService.delete(persDelete);
			}
			else {
				persDelete.setRuta(null);
				persDelete.setStatus("I");
				personalService.store(persDelete);
			}
					
		}
		catch(DataAccessException e) {
			
			response.put("mensaje", "Error, no fue posible eliminar al personal");
			response.put("err", persDelete.getNombre().concat("No fue eliminado"));
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);	
		}
		
		response.put("mensaje","Registro eliminado correctamente");
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.NO_CONTENT);	
	}
	
	@Secured("ROLE_ADMIN")
	@GetMapping("/personal/find-usuario/{id}")
	public ResponseEntity<?> verificarExisteUsuario(@PathVariable Long id){
		Map<String, Object> response= new HashMap<>();	
		Personal personal=personalService.findById(id);
		Usuario usuario=usuarioService.findByPersonal(personal);
		if(usuario==null) {
			response.put("existe", "NO");
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.ACCEPTED);
		}
		
		response.put("existe", "SI");
		
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.ACCEPTED);
		
	}
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping("/personal/find-by-ruta/{ruta}")
	public Personal findPersonalByRuta(@PathVariable String ruta){
		return personalService.findPersonalByRuta(ruta);
	}

}