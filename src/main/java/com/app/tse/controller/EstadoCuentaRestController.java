package com.app.tse.controller;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.tse.model.entity.Credito;
import com.app.tse.model.entity.EstadoCuenta;
import com.app.tse.model.entity.FechaPago;
import com.app.tse.model.entity.Usuario;
import com.app.tse.model.service.ICreditoService;
import com.app.tse.model.service.IEstadoCuentaService;
import com.app.tse.model.service.IFechaPagoService;

@RestController
@RequestMapping("/api-credito")
public class EstadoCuentaRestController {
	
	@Autowired
	private IEstadoCuentaService estadoCuentaService;
	@Autowired
	private IFechaPagoService fechaPagoService;
	@Autowired
	private ICreditoService creditoService;
	
	Usuario us = new Usuario();

	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping("/estado-cuenta/page/{page}")
	public Page<EstadoCuenta> findAll(@PathVariable Integer page){
		return estadoCuentaService.findAll(PageRequest.of(page,4));
	}
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping("/estado-cuenta")
	public List<EstadoCuenta> findAll(){
		return estadoCuentaService.findAll();
	}
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping("/estado-cuenta/{ubicacion}")
	public List<EstadoCuenta> findAllByUbicacion(@PathVariable String ubicacion){
		return estadoCuentaService.findAllByUbicacion(ubicacion);
		
	}
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping("/estado-cuenta/mora/{ubicacion}")
	public List<EstadoCuenta> findAllByMoraUbicacion(@PathVariable String ubicacion){
		
		return estadoCuentaService.findAllByMoraUbicacion(ubicacion);
	}
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping("/estado-cuenta/reporte/ruta/{ubicacion}")
	public List<EstadoCuenta> findAllByUbicacionForReporte(@PathVariable String ubicacion){
		
		return estadoCuentaService.findAllByUbicacionForReporte(ubicacion,LocalDate.now(ZoneId.of("America/Guatemala")));
	}
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping("/estado-cuenta/adelantado/{ubicacion}")
	public List<EstadoCuenta> findAllByAdelantadoUbicacion(@PathVariable String ubicacion){
		return estadoCuentaService.findAllByAdelantadoUbicacion(ubicacion);
	}
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping("/estado-cuenta/fecha-ubicacion/{ubicacion}")
	public List<EstadoCuenta> findAllByFechaVencimiento(@PathVariable String ubicacion){

		return estadoCuentaService.findAllByFechaVencimiento(ubicacion,LocalDate.now(ZoneId.of("America/Guatemala")));
	}
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping("/estado-cuenta/fechas/pago/{id}")
	public List<FechaPago> findAllFechaByCredito(@PathVariable Long id){
		
		Credito credito= creditoService.findById(id);
		return fechaPagoService.findAllByCredito(credito);
	}

}