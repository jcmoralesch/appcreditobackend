package com.app.tse.controller;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.tse.model.entity.Credito;
import com.app.tse.model.entity.EstadoCuenta;
import com.app.tse.model.entity.FechaPago;
import com.app.tse.model.entity.Mora;
import com.app.tse.model.entity.Record;
import com.app.tse.model.entity.Usuario;
import com.app.tse.model.service.ICreditoService;
import com.app.tse.model.service.IEstadoCuentaService;
import com.app.tse.model.service.IFechaPagoService;
import com.app.tse.model.service.IRecordService;
import com.app.tse.model.service.UsuarioService;
import com.app.tse.utileria.IGestionarFechaService;

@RestController
@RequestMapping("/api-credito")
public class CreditoRestController {
	
	@Autowired
	private ICreditoService creditoService;
	@Autowired
	private IRecordService recordService;
	@Autowired
	private IEstadoCuentaService estadoCuentaService;
	@Autowired
	private UsuarioService usuarioService;
	@Autowired
	private IGestionarFechaService gestionarFechaService;
	@Autowired
	private IFechaPagoService fechaPagoService;
	
	@PostMapping("/credito")
	public ResponseEntity<?> store(@Valid @RequestBody Credito credito,OAuth2Authentication authentication){
		
		Credito creditoNew =null;
		EstadoCuenta estadoCuenta = new EstadoCuenta();
		Mora mora = new Mora();
		Map<String, Object> response= new HashMap<>();
		List<LocalDate> dates=null;
		int idFechaVencimiento=0;
		Usuario usuario = usuarioService.findByUsername(authentication.getName());
		
		Double montoTotal=credito.getMonto()+(credito.getMonto()*credito.getTipoPago().getInteres());
		Double couta=montoTotal/credito.getTipoPago().getNoPago();
		
		estadoCuenta.setSaldo(montoTotal);
		estadoCuenta.setPagosFaltantes(credito.getTipoPago().getNoPago());
		
		dates=gestionarFechaService.generarFechas(credito.getTipoPago().getNoPago(),
                credito.getTipoPago().getFormaPago(),LocalDate.now(ZoneId.of("America/Guatemala")));
		 
		for (LocalDate f : dates) {
			idFechaVencimiento=dates.lastIndexOf(f);
		}
			
		try {
			
			credito.setMontoTotal(montoTotal);
			credito.setCuota(couta);
			credito.setFechaVencimiento(dates.get(idFechaVencimiento));
			
			List<FechaPago> fechasPago = new ArrayList<FechaPago>();
			for (LocalDate localDate : dates) {
				FechaPago fechaPago= new FechaPago();
				fechaPago.setFecha(localDate);	
				fechaPago.setCredito(credito);
				fechasPago.add(fechaPago);	
			}
			
			credito.addMora( mora);
			credito.setUsuario(usuario);
			credito.setFechaPago(fechasPago);
			creditoNew =creditoService.store(credito);		
			estadoCuenta.setCredito(creditoNew);		
			estadoCuentaService.store(estadoCuenta);
							
		}catch(DataAccessException e) {
			
			response.put("mensaje","Error al realizar el insert en la BBDD");
			response.put("errors",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje","Credito registrado con exito");
		response.put("credito",creditoNew);
		
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
		
	}
	
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping("/credito/{ubicacion}")
	public List<Credito> findCreditoByUbicacion(@PathVariable String ubicacion){

		return creditoService.findCreditoByUbicacion(ubicacion,LocalDate.now(ZoneId.of("America/Guatemala")));
	}
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping("/credito/ruta/{ruta}")
	public List<Credito> findCreditoByRuta(@PathVariable String ruta){
		
		return creditoService.findCreditoByRuta(ruta,LocalDate.now(ZoneId.of("America/Guatemala")));
	}
	
	@Secured({"ROLE_ADMIN"})
	@GetMapping("/credito/consultar/{fecha1}/{fecha2}/{ubicacion}/{ruta}")
	public List<Credito> findByDates(@PathVariable("fecha1") String fecha1, @PathVariable("fecha2")String fecha2,
			              @PathVariable("ubicacion")String ubicacion,@PathVariable("ruta")String ruta){
		
		LocalDate f1=LocalDate.parse(fecha1);
		LocalDate f2= LocalDate.parse(fecha2);
		
		if(ruta.equals("undefined")) {
			ruta="";
		}
		if(ubicacion.equals("undefined")) {
			ubicacion="";
		}
		
		if(ubicacion.equals("") && ruta.equals("")) {
			
			return creditoService.findByFechaPagoBeetween(f1, f2); 
		}
		
		if(ubicacion!="" && ruta!="") {
			
			return creditoService.findCreditoByDateAndUbicacionAndRuta(f1, f2, ubicacion, ruta);
		}
		
	   if(ubicacion!="" && ruta.equals("")) {
			return creditoService.findByDateAndUbicacion(f1, f2, ubicacion);
			
		}
	   
	   if(ubicacion.equals("") && ruta!="") {
		   return creditoService.findCreditoByDateAndUbicacionAndRuta(f1, f2, ubicacion, ruta);
	   }
		
		return null;
	}
	
		
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@PutMapping("/credito/update/{id}")
	public Credito update(@PathVariable Long id,@RequestBody Credito credito,OAuth2Authentication authentication){
		 Credito creditoActualizado=creditoService.findById(id);
		 EstadoCuenta eCActualizado=estadoCuentaService.findByIdCredito(id);
	     fechaPagoService.deleteFechaInBatch(fechaPagoService.findAllByCreditoId(credito.getId()));
		 int idFechaVencimiento=0;
		 List<LocalDate> dates=null;
		 Usuario usuario = usuarioService.findByUsername(authentication.getName());
			
		 Double montoTotal=credito.getMonto()+(credito.getMonto()*credito.getTipoPago().getInteres());
		 Double cuota=montoTotal/credito.getTipoPago().getNoPago();
		 
		 dates=gestionarFechaService.generarFechas(credito.getTipoPago().getNoPago(),
	                credito.getTipoPago().getFormaPago(),LocalDate.now(ZoneId.of("America/Guatemala")));
			 
			for (LocalDate f : dates) {
				idFechaVencimiento=dates.lastIndexOf(f);
			}
			
			List<FechaPago> fechasPago = new ArrayList<FechaPago>();
			for (LocalDate localDate : dates) {
				FechaPago fechaPago= new FechaPago();
				fechaPago.setFecha(localDate);	
				fechaPago.setCredito(credito);
				fechasPago.add(fechaPago);	
			}
		 
		 creditoActualizado.setMonto(credito.getMonto());
		 creditoActualizado.setFechaRegistro(LocalDate.now(ZoneId.of("America/Guatemala")));
		 creditoActualizado.setMontoTotal(montoTotal);
		 creditoActualizado.setCuota(cuota);
		 creditoActualizado.setUsuario(usuario);
		 creditoActualizado.setTipoPago(credito.getTipoPago());
		 creditoActualizado.setFechaPago(fechasPago);
		 creditoActualizado.setFechaVencimiento(dates.get(idFechaVencimiento));
		 eCActualizado.setSaldo(montoTotal);
		 eCActualizado.setPagosFaltantes(credito.getTipoPago().getNoPago());
		 
		 estadoCuentaService.store(eCActualizado);
		 
		 return creditoService.store(creditoActualizado);	 
	}
	
	@Secured("ROLE_ADMIN")
	@GetMapping("/credito/find/status/active")
	public List<Credito> findAllStatusA(){
		return creditoService.findAllStatusA();
	}
	
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@PutMapping("/credito/update/fecha-ciclo/{id}")
	public Credito updateDateAndCiclo(@PathVariable Long id,@RequestBody Credito credito,OAuth2Authentication authentication){
		
		 Credito creditoActualizado=creditoService.findById(id);
	     fechaPagoService.deleteFechaInBatch(fechaPagoService.findAllByCreditoId(credito.getId()));
		 int idFechaVencimiento=0;
		 List<LocalDate> dates=null;
		 Usuario usuario = usuarioService.findByUsername(authentication.getName());
		 Record recordUpdated=recordService.findById(credito.getCliente().getRecord().getId());
			
		 dates=gestionarFechaService.generarFechas(credito.getTipoPago().getNoPago(),
	                credito.getTipoPago().getFormaPago(),credito.getFechaRegistro());
			 
			for (LocalDate f : dates) {
				idFechaVencimiento=dates.lastIndexOf(f);
			}
			
			List<FechaPago> fechasPago = new ArrayList<FechaPago>();
			for (LocalDate localDate : dates) {
				FechaPago fechaPago= new FechaPago();
				fechaPago.setFecha(localDate);	
				fechaPago.setCredito(credito);
				fechasPago.add(fechaPago);	
			}
			
		recordUpdated.setCiclo(credito.getCliente().getRecord().getCiclo());
			
		 creditoActualizado.setFechaPago(fechasPago);
		 creditoActualizado.setUsuario(usuario);
		 creditoActualizado.setFechaVencimiento(dates.get(idFechaVencimiento));
		 creditoActualizado.setFechaRegistro(credito.getFechaRegistro()); 
		 
		 recordService.store(recordUpdated);
		 return creditoService.store(creditoActualizado);	 
	}
	
	@Secured("ROLE_ADMIN")
	@GetMapping("/credito/find/status/active-ruta/{ruta}")
	public List<Credito> findAllStatusAAndRuta(@PathVariable String ruta){
		return creditoService.findAllStatusAAndRuta(ruta);
	}
}
