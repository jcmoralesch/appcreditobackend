package com.app.tse.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.tse.model.entity.Ruta;
import com.app.tse.model.service.IRutaService;


@RestController
@RequestMapping("/api-ruta")
public class RutaRestController {
	
	@Autowired
	private IRutaService rutaService;
	
	@Secured("ROLE_ADMIN")
	@PostMapping("/ruta")
	public ResponseEntity<?> store(@Valid @RequestBody Ruta ruta,BindingResult errores){
		
		Ruta rutaNew =null;
		Map<String, Object> response = new HashMap<>();
		
		if(errores.hasErrors()) {
			List<String> errors=errores.getFieldErrors().stream().map(err->"El campo " + err.getField()+
					" " + err.getDefaultMessage()).collect(Collectors.toList());
			response.put("errors",errors);
			
			return new ResponseEntity<Map<String,Object>>(response, HttpStatus.BAD_REQUEST);
		}
		
		try {
			
			ruta.setCodigo(ruta.getCodigo().toUpperCase());
			ruta.setUbicacion(ruta.getUbicacion().toUpperCase());
			
			rutaNew = rutaService.store(ruta);
			
		}
		catch(DataIntegrityViolationException ex) {
			
			response.put("mensaje","Error al registrar");
			response.put("err","La ruta "+ruta.getCodigo()+" ya esta Registrada");
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		catch (DataAccessException e) {
			response.put("mensaje","Error al realizar en insert en la BBDD");
			response.put("err",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	    response.put("mensaje", "Cliente registrado con exito");
	    response.put("ruta", rutaNew);
		
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
	}
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})	
	@GetMapping("/ruta/page/{page}")
	public Page<Ruta> findAll(@PathVariable Integer page){
		return rutaService.findAll(PageRequest.of(page, 4));
	}
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping("/ruta")
	public List<Ruta> findAllRuta(){
		return rutaService.findAllRuta();
	}
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping("/ruta/distinct")
	public List<String> findRutaDistinct() {
		
		return rutaService.findRutaDistinct();
	}
	
	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping("/ruta/{ubicacion}")
	public List<Ruta> findRutaByUbicacion(@PathVariable String ubicacion){
		return rutaService.findRutaByUbicacion(ubicacion);
	}

}