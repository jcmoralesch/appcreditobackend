package com.app.tse.utileria;

import java.time.LocalDate;
import java.util.List;

public interface IGestionarFechaService {
	
	public  List<LocalDate> generarFechasDiaras(int dias,LocalDate fecha);
	public  List<LocalDate> generarFechasSemanales(int dias,LocalDate fecha);
	public  List<LocalDate> generarFechas(int noPago,String formaPago, LocalDate fecha);
}
