package com.app.tse.utileria;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class GestionarFechaServiceImpl implements IGestionarFechaService{

	@Override
	@Transactional
	public List<LocalDate> generarFechasDiaras(int dias,LocalDate fecha) {
		LocalDate sumarDias=null;
		int i=0;
		boolean contador=true;
		List<LocalDate> dato = new ArrayList<LocalDate>();
			
		if(fecha.getDayOfWeek().toString()=="FRIDAY") {
			sumarDias= fecha.plusDays(2);
		}else{
			sumarDias= fecha.plusDays(1);
		}	
		
		while(contador) {
			
			if(sumarDias.plusDays(i).getDayOfWeek().toString()!="SATURDAY" && 
			   sumarDias.plusDays(i).getDayOfWeek().toString()!="SUNDAY"){
				
				dato.add(sumarDias.plusDays(i));				
			}
			i++;
			
			if(dato.size()==dias) {
				contador=false;
			}
		}
		
		return dato;
	}

	@Override
	@Transactional
	public List<LocalDate> generarFechasSemanales(int dias,LocalDate fecha) {
		boolean contador=true;
		int i=7;
		List<LocalDate> dato = new ArrayList<LocalDate>();
			
		while(contador) {
					
		    dato.add(fecha.plusDays(i));				
			i+=7;
			if(dato.size()==dias) {
				contador=false;
			}
		}
		
		return dato;
	}

	@Override
	@Transactional
	public List<LocalDate> generarFechas(int noPago, String formaPago,LocalDate fecha) {
        List<LocalDate>fechas=null;
		
		if(formaPago.equals("DIARIO")) {
			fechas=generarFechasDiaras(noPago,fecha);
			
		}else if(formaPago.equals("SEMANAL")) {
			fechas=generarFechasSemanales(noPago,fecha);
		}	
				
		return fechas;
	}
}
