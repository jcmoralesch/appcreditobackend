package com.app.tse.auth;

public class JwtConfig {
	
	public static final String LLAVE_SECRETA ="alguna.clave.secreta.123456789";
	
	public static final String RSA_PRIVADA="-----BEGIN RSA PRIVATE KEY-----\n" + 
			"MIIEowIBAAKCAQEAv6ysxY7QN0eXSMoP3yh622iaUNbo76Sm+K9knnJi3pUObwfO\n" + 
			"x3BBlrd1cQKvEQBGCzJQ9BsLjT1oXz5S+nJ40isYB405aM28BaN+3UlatYlJ438l\n" + 
			"GHyrYPIZlg3t/soMEeiUv/eQSpnJjCrd8+6ImwSy0aJ0IEyJQmaeUL6Pue2cd1/f\n" + 
			"0DqT0LhPa8Bm7aZhLkxzuKkTIBpjVffOCoUQicsHZQqIhP1dSi8HowUz/xy+XM1y\n" + 
			"fo87+TAoSo8tpfSXBo9HHMPGvlE4MjVAQy+T0t6q1IDi4lXjeT6thZxSSbI7YLso\n" + 
			"mjUC/w+oCdVmXVJ+v4OHeCq21LAz5Lsnao99WwIDAQABAoIBAC4993S2Dq+3et0Z\n" + 
			"upiJCJLUep5UKuBd5daLYggD6qxuY4wP4ygkgf5dhlzcW+YLHdrnqbYqrC/IB90q\n" + 
			"iRRY96MkuGhh/t60+86Uci/q2ZKAa64R554vvZzm65tTAv78Ocs8RF7H2CzXS1Og\n" + 
			"p+mIu7ZCfO2sdr2wGVFGtp0eTEh7iujXto+CiISX4mGqWSjy+bWXnFCzkAAEp03O\n" + 
			"ABbvW9AiBNsxqOJ9VZal/1Yzr1vnHkYI9gBpEktkCyg2PHXluHHcwtQlaT9kkZAz\n" + 
			"4gStWr4cR9vncECPL0bHuQfu+QamLBK5ulgsKDxeouZ32OMlYW570z48IjxzKxt7\n" + 
			"dg8QLjkCgYEA3bQcdeCf/CmrX29jpgxERXHLtVUNUU5Ak7FOILANJqITl3NPFfJ0\n" + 
			"isiCZQUhikGq+I3c4puqQOYCZWtQYfThclMVOV9mAzvmQrWqTAFSwLx+kAVft6Qr\n" + 
			"1OLemIbQDk9UbqRckoJCJqi9YnoHlddy/a0dAgCWc3bqB2Alcu/bhR0CgYEA3VNa\n" + 
			"/3mNSGDKR0e4h0A6vJp+/x3f6TQjKyHyIn+KBQR9u1fwdNpa96ZKHJUQkYpvKb0D\n" + 
			"HdyHtSt1za8Fd7zkNy6bo6waPWJcsfTaUfJwPTrSUYAL8IyvUE7sXbtkxYvsMK+l\n" + 
			"eHLveFj+uG/g/tR+VMIzdWNh7t7xAcdVB0G42tcCgYBNsBCNCeJl5WAk+A5uIDxI\n" + 
			"LWEZsK0TAZ3FY0siTy68i+zblE9xPF20SCDRL2QaY3p60JNHYQ7e4PqT+W/Yc4wQ\n" + 
			"mKaGTTSdC39bfvcm3QvXd/XNl+R7oG6vkbQ4tHzxlsDaaIP73uM5UsXSXTqNOksU\n" + 
			"M3ZHvlb6LQBDB2D4Dq7YzQKBgDnBXMe6IoI2r9J2BtNUkX9SGb3dP+cNMfg5fYQ/\n" + 
			"H/c7Osw+oIL90Cs7+68FvzMbmvKbXkrWl5tRYgZwm86pPiygPwExEg/esS3HJYmo\n" + 
			"6F0yElwuO+9j5wPf9JJpKlzarKHv1BBgfdk78AyVNXOKZAUeQGtrqcy55M6AA9JG\n" + 
			"9kQ1AoGBAJFrWFV55XQWh6N1tUcN/YX2XP9KCpjEXqrxGcaXN+LlAkzsbPrmC9Fy\n" + 
			"9gB2g42RQKBDCuHTWmc09dSXESck45zz1y79N7drWDM7rd2Qup4X2s3cyBBJvuiW\n" + 
			"70sMPzJ+K1O2DdFoZHUjBw54IFyMpAO3cNLzE91hLvvI4M394IDn\n" + 
			"-----END RSA PRIVATE KEY-----";
	
	public static final String RSA_PUBLICA="-----BEGIN PUBLIC KEY-----\n" + 
			"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAv6ysxY7QN0eXSMoP3yh6\n" + 
			"22iaUNbo76Sm+K9knnJi3pUObwfOx3BBlrd1cQKvEQBGCzJQ9BsLjT1oXz5S+nJ4\n" + 
			"0isYB405aM28BaN+3UlatYlJ438lGHyrYPIZlg3t/soMEeiUv/eQSpnJjCrd8+6I\n" + 
			"mwSy0aJ0IEyJQmaeUL6Pue2cd1/f0DqT0LhPa8Bm7aZhLkxzuKkTIBpjVffOCoUQ\n" + 
			"icsHZQqIhP1dSi8HowUz/xy+XM1yfo87+TAoSo8tpfSXBo9HHMPGvlE4MjVAQy+T\n" + 
			"0t6q1IDi4lXjeT6thZxSSbI7YLsomjUC/w+oCdVmXVJ+v4OHeCq21LAz5Lsnao99\n" + 
			"WwIDAQAB\n" + 
			"-----END PUBLIC KEY-----";
	
	

}
