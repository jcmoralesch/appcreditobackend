package com.app.tse.auth;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

import com.app.tse.model.entity.AccesoSistema;
import com.app.tse.model.entity.Usuario;
import com.app.tse.model.service.IAccesoSistemaService;
import com.app.tse.model.service.IUsuarioService;

@Component
public class InfoAdicionalToken implements TokenEnhancer{
	
	@Autowired
	private IUsuarioService usuarioService;
	@Autowired
	private IAccesoSistemaService accesoSistemaService;

	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
		
		Usuario usuario=usuarioService.findByUsername(authentication.getName());
		AccesoSistema accesoSistema= new AccesoSistema();
		
		Map<String,Object> info = new HashMap<>();
		info.put("info_adicional","Hola que tal!: ".concat(authentication.getName()));
		info.put("id_usuario",usuario.getId());
		
		accesoSistema.setUsario(usuario);
		accesoSistemaService.store(accesoSistema);
		
		((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(info);
		return accessToken;
	}

}
