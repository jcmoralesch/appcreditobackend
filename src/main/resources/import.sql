insert into rutas(codigo,ubicacion) values('AGENCIAUS','USPANTAN');

insert into tipo_pagos(nombre_tipo,no_pago,interes,plazo,forma_pago) values('Diario 25',25,0.25,'25 Dias','DIARIO');

insert into personals(identificacion,nombre,apellido,direccion,telefono,status,ruta_id,create_at) values('2652 70243 1415','ADÁN GABRIEL','TOMAS DAMIAN','USPANTÁN','31534720','A',1,'2019-05-06');

insert into usuarios (username,password,enabled,status,personal_id) values('Gabriel','$2a$10$VI9EKCr7uZ2g5NUPC3g1tOtvpeSStDS3TLbCTB8eRbZ.XClcFRWCK',1,'Activo',1);

insert into roles(nombre) values('ROLE_ADMIN');
insert into roles(nombre) values('ROLE_USER');

insert into usuarios_role(usuario_id,role_id)values(1,1);
